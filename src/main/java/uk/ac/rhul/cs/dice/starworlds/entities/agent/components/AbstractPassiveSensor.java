package uk.ac.rhul.cs.dice.starworlds.entities.agent.components;

import java.util.HashSet;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.annotations.SensiblePerception;
import uk.ac.rhul.cs.dice.starworlds.perception.NullPerception;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

@SensiblePerception({ NullPerception.class })
public abstract class AbstractPassiveSensor<K extends ComponentKey> extends AbstractComponent<K> implements Sensor<K> {

	private Set<Perception> perceptions;

	// private boolean open = false;

	/**
	 * Constructor.
	 */
	public AbstractPassiveSensor(K componentKey) {
		super(componentKey);
		this.perceptions = new HashSet<>();
	}

	@Override
	public void notify(Perception perception) {
		this.perceptions.add(perception);
		this.getBody().sensorActive(this);
	}

	/**
	 * Getter for the {@link Perception}s currently stored in the internal buffer of this {@link AbstractActiveSensor}.
	 * These are all of the perceptions that were received by this {@link AbstractActiveSensor} in the previous cycle.
	 * The internal buffer of {@link Perception}s will be cleared after this method call.
	 * 
	 * @return the set of {@link Perception}s
	 */
	@Override
	public Set<Perception> getPerceptions() {

		HashSet<Perception> result = new HashSet<>(this.perceptions);
		this.perceptions.clear();
		return result;
	}

	// @Override
	// public boolean isOpen() {
	// return open;
	// }
	//
	// @Override
	// public void setClosed() {
	// this.open = false;
	// }
	//
	// @Override
	// public void setOpen() {
	// this.open = true;
	// }

}
