package uk.ac.rhul.cs.dice.starworlds.environment.subscription;

import java.util.Collection;

public interface SubcriptionHandler<T, S> {

	public void subscribe(T arg);

	public void unsubscribe(T arg);

	public void subscribe(T arg1, S arg2);

	public void unsubscribe(T arg1, S arg2);

	public Collection<S> subscribed(T arg);

}
