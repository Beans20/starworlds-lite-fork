package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard;

import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;

public class AvatarsAttribute extends MapAttribute<String, AbstractAvatarAgent<?>> {
	public AvatarsAttribute(Map<String, AbstractAvatarAgent<?>> attribute) {
		super(attribute);
	}

	@Override
	public String toString() {
		return new StringBuilder("Avatars: ").append(System.lineSeparator()).append(super.toString("  ")).toString();
	}
}
