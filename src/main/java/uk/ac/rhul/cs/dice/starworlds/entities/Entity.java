package uk.ac.rhul.cs.dice.starworlds.entities;

/**
 * The interface for physical objects implementing {@link Manifest}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface Entity extends Manifest {
}