package uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.perception.CommunicationPerception;

public class CommunicationActionRuleSet<A extends CommunicationAction, E extends Environment<E>> implements
		ActionRuleSet<A, E> {

	@Override
	public boolean isPossible(A action, E environment) {
		return true;
	}

	@Override
	public boolean perform(A action, E environment) {
		CommunicationPerception perception = new CommunicationPerception(action.getMessage());
		if (!action.getRecipientsIds().isEmpty()) {
			environment.notifySensors(perception, action.getRecipientsIds());
		} else {
			environment.notifySensors(perception);
		}
		return true;
	}

	@Override
	public boolean verify(A action, E environment) {
		return true;
	}
}
