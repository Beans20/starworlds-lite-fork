package uk.ac.rhul.cs.dice.starworlds.entities.agent.concrete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgentMind;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimpleComponentKey;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.AbstractAmbient;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter.FilterChain;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter.MapSearchFilter;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

public class SimpleMind extends AbstractAgentMind<SimpleComponentKey> {

	private List<String> recipients = new ArrayList<>();
	private Collection<Perception> perceptions;
	private Map<SimpleComponentKey, EnvironmentalAction> actions;

	public SimpleMind() {
		actions = new HashMap<>();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

	@Override
	public void perceive() {
		perceptions = this.getBody().perceive();
		// System.out.println("PERCEIVE: " + perceptions);
	}

	@Override
	public void decide() {
		actions.clear();
		actions = new HashMap<>();
		// actions.put(SimpleComponentKey.ACTUATOR, new CommunicationAction("Hello"));

		List<String> searchFor = new ArrayList<>();
		searchFor.add("2");
		searchFor.add("3");
		searchFor.add("1");
		FilterChain<MapAttribute<?, ?>, Collection<?>> chain = FilterChain.start(AbstractAmbient.AGENTSKEY).append(
				new MapSearchFilter(searchFor));
		actions.put(SimpleComponentKey.ACTIVESENSOR, new SenseAction(chain));
		// System.out.println("DECIDE");
	}

	@Override
	public void execute() {
		this.getBody().attempt(actions);
	}
}
