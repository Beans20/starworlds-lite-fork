package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute;

import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;

public class AttributeException extends StarWorldsRuntimeException {

	private static final long serialVersionUID = -8443482584280515420L;

	public AttributeException() {
		super();
	}

	public AttributeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AttributeException(String message, Throwable cause) {
		super(message, cause);
	}

	public AttributeException(String message) {
		super(message);
	}

	public AttributeException(Throwable cause) {
		super(cause);
	}

}
