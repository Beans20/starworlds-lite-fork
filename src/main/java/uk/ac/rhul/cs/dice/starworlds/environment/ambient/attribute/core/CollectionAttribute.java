package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core;

import java.util.Collection;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.Attribute;
import uk.ac.rhul.cs.dice.starworlds.utils.StringUtils;

public class CollectionAttribute<V> implements Attribute, Collection<V> {

	private Collection<V> collection;

	public CollectionAttribute(Collection<V> collection) {
		this.collection = collection;
	}

	public boolean add(V e) {
		return collection.add(e);
	}

	public boolean addAll(Collection<? extends V> c) {
		return collection.addAll(c);
	}

	public void clear() {
		collection.clear();
	}

	public boolean contains(Object o) {
		return collection.contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return collection.containsAll(c);
	}

	public boolean equals(Object o) {
		return collection.equals(o);
	}

	public void forEach(Consumer<? super V> arg0) {
		collection.forEach(arg0);
	}

	public int hashCode() {
		return collection.hashCode();
	}

	public boolean isEmpty() {
		return collection.isEmpty();
	}

	public Iterator<V> iterator() {
		return collection.iterator();
	}

	public Stream<V> parallelStream() {
		return collection.parallelStream();
	}

	public boolean remove(Object o) {
		return collection.remove(o);
	}

	public boolean removeAll(Collection<?> c) {
		return collection.removeAll(c);
	}

	public boolean removeIf(Predicate<? super V> filter) {
		return collection.removeIf(filter);
	}

	public boolean retainAll(Collection<?> c) {
		return collection.retainAll(c);
	}

	public int size() {
		return collection.size();
	}

	public Spliterator<V> spliterator() {
		return collection.spliterator();
	}

	public Stream<V> stream() {
		return collection.stream();
	}

	public Object[] toArray() {
		return collection.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return collection.toArray(a);
	}

	@Override
	public String toString() {
		return new StringBuilder(this.getClass().getSimpleName()).append("{")
				.append(StringUtils.collectionToString(collection)).append("}").toString();
	}

}
