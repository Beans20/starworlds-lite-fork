package uk.ac.rhul.cs.dice.starworlds.environment.subscription;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class InheritanceMap<T> implements Map<Class<?>, Collection<T>> {

	protected Map<Class<?>, Collection<T>> map;
	protected Set<Class<?>> base;

	public InheritanceMap(Class<?>... base) {
		this.base = new HashSet<>(Arrays.asList(base));
		this.map = new HashMap<>();
		this.base.add(Object.class);
	}

	public InheritanceMap(Set<Class<?>> base) {
		this.base = base;
		this.map = new HashMap<>();
		this.base.add(Object.class);
	}

	public InheritanceMap(Class<?> base) {
		this.base = new HashSet<>();
		this.map = new HashMap<>();
		this.base.add(base);
		this.base.add(Object.class);
	}

	/**
	 * Initialises base types. Should be called in a subclass constructor.
	 */
	protected void initialiseBase() {
		this.base.forEach(b -> this.map.put(b, new InheritanceCollection<>(newCollection(b))));
	}

	public Set<Class<?>> getBase() {
		return base;
	}

	/**
	 * Gets the {@link Class} that given value was inherited from. The returned {@link Class} is a super {@link Class}
	 * of the given class. If the value is not inherited from any super class, the given class will be returned. If the
	 * given value does not belong to the given class null will be returned.
	 * 
	 * @param cls
	 *            : to check
	 * @param value
	 *            : inherited
	 * @return the super {@link Class} that value was inherited from
	 */
	public Class<?> inheritedFrom(Class<?> cls, T value) {
		InheritanceCollection<T> col = this.get(cls);
		if (col != null) {
			if (col.nonInheritedContains(value)) {
				return cls;
			} else {
				return inheritedFrom(cls.getSuperclass(), value);
			}
		} else {
			return null;
		}
	}

	@Override
	public InheritanceCollection<T> put(Class<?> key, Collection<T> value) {
		InheritanceCollection<T> icol;
		if (!map.containsKey(key)) {
			icol = new InheritanceCollection<>(value);
			map.put(key, icol);
			icol.setInherited(putNext(key.getSuperclass()));
		} else {
			icol = (InheritanceCollection<T>) map.get(key);
			icol.addAll(value);
		}
		return icol;
	}

	private InheritanceCollection<T> putNext(Class<?> key) {
		if (!map.containsKey(key)) {
			InheritanceCollection<T> icol = new InheritanceCollection<>(newCollection(key));
			map.put(key, icol);
			icol.setInherited(putNext(key.getSuperclass()));
			return icol;
		} else {
			return get(key);
		}
	}

	protected abstract Collection<T> newCollection(Class<?> key);

	@Override
	public void putAll(Map<? extends Class<?>, ? extends Collection<T>> m) {
		m.entrySet().forEach(e -> this.put(e.getKey(), e.getValue()));
	}

	@Override
	public InheritanceCollection<T> remove(Object key) {
		InheritanceCollection<T> remove = this.get(key);
		remove.clear();
		return remove;
	}

	@Override
	public void clear() {
		this.map.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return this.map.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return this.map.containsValue(value);
	}

	@Override
	public Set<Entry<Class<?>, Collection<T>>> entrySet() {
		return this.map.entrySet().stream()
				.map(e -> new SimpleEntry<Class<?>, Collection<T>>(e.getKey(), e.getValue()))
				.collect(Collectors.toSet());
	}

	@Override
	public InheritanceCollection<T> get(Object key) {
		return (InheritanceCollection<T>) this.map.get(key);
	}

	@Override
	public boolean isEmpty() {
		return this.map.isEmpty();
	}

	@Override
	public Set<Class<?>> keySet() {
		return this.map.keySet();
	}

	@Override
	public int size() {
		return this.map.size();
	}

	@Override
	public Collection<Collection<T>> values() {
		return this.map.values();
	}

	@Override
	public String toString() {
		return this.map.toString();
	}
}
