package uk.ac.rhul.cs.dice.starworlds.perception.factory;

public interface PerceptionFactory<P> {

	public P getPerception(Object... args);

}
