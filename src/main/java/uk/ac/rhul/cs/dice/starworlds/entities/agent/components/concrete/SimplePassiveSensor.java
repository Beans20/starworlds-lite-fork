package uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete;

import uk.ac.rhul.cs.dice.starworlds.annotations.SensiblePerception;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.AbstractPassiveSensor;
import uk.ac.rhul.cs.dice.starworlds.perception.CommunicationPerception;

@SensiblePerception({ CommunicationPerception.class })
public class SimplePassiveSensor extends AbstractPassiveSensor<SimpleComponentKey> {

	public SimplePassiveSensor() {
		super(SimpleComponentKey.PASSIVESENSOR);
	}
}
