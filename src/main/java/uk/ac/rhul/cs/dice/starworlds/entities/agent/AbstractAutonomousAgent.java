package uk.ac.rhul.cs.dice.starworlds.entities.agent;

import java.util.Collection;

import uk.ac.rhul.cs.dice.starworlds.appearances.ActiveBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Actuator;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Component;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Sensor;

public abstract class AbstractAutonomousAgent<K extends ComponentKey> extends AbstractAgent<K> {

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            :
	 * @param components
	 *            : collection of {@link Sensor}s and {@link Actuator}s to be used by this {@link ActiveBody}
	 * @param mind
	 *            : the {@link AbstractAgentMind}.
	 * 
	 */
	public AbstractAutonomousAgent(String id, Collection<Component<K>> components, AbstractAgentMind<K> mind) {
		super(id, components, mind);
	}

	/**
	 * Constructor.
	 * 
	 * @param appearance
	 *            : the {@link ActiveBodyAppearance}.
	 * @param components
	 *            : collection of {@link Sensor}s and {@link Actuator}s to be used by this {@link ActiveBody}
	 * @param mind
	 *            : the {@link AbstractMind}.
	 */
	public AbstractAutonomousAgent(String id, ActiveBodyAppearance appearance, Collection<Component<K>> components,
			AbstractMind<K> mind) {
		super(id, appearance, components, mind);
	}

	/**
	 * Returns the {@link Mind} of the {@link AbstractAutonomousAgent}.
	 * 
	 * @return the {@link Mind} of the {@link AbstractAutonomousAgent}.
	 */
	@Override
	public AbstractAgentMind<K> getMind() {
		return (AbstractAgentMind<K>) this.mind;
	}
}