package uk.ac.rhul.cs.dice.starworlds.environment.concrete;

import uk.ac.rhul.cs.dice.starworlds.appearances.EnvironmentAppearance;
import uk.ac.rhul.cs.dice.starworlds.environment.AbstractEnvironment;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.Universe;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.AbstractAmbient;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.Ambient;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.Physics;
import uk.ac.rhul.cs.dice.starworlds.environment.subscription.SensorSubscriptionHandler;
import uk.ac.rhul.cs.dice.starworlds.initialisation.IDFactory;

/**
 * A simple example of a {@link Universe}. Take note of the generic type {@link SimpleUniverse} in the extension of
 * {@link AbstractEnvironment} and implementation of {@link Universe}.
 * 
 * @author Ben Wilkins
 *
 */
public class SimpleUniverse extends AbstractEnvironment<SimpleUniverse> implements Universe<SimpleUniverse> {

	/**
	 * Constructor.
	 *
	 * @param ambient
	 *            : a {@link Ambient} instance
	 * @param physics
	 *            : the {@link Physics} of the environment
	 * @param subscriptionHandler
	 *            : the {@link AbstractSubscriptionHandler} that will handle subscriptions in this {@link Environment}.
	 */
	public SimpleUniverse(AbstractAmbient ambient, SimplePhysics physics, SensorSubscriptionHandler subscriptionHandler) {
		super(IDFactory.getInstance().getNewID(), SimpleUniverse.class, ambient, physics, subscriptionHandler,
				new EnvironmentAppearance());
	}

	@Override
	public void postInitialisation() {
	}

	@Override
	public Boolean isSimple() {
		return true;
	}

	@Override
	public void simulate() {
		physics.simulate();
	}

	@Override
	public void run() {
		this.simulate();
	}

}
