package uk.ac.rhul.cs.dice.starworlds.entities.avatar.concrete;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;
import uk.ac.rhul.cs.dice.starworlds.actions.factory.CommunicationActionFactory;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimpleComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.KeyboardAvatarMind;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.UserResponse;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;

public class CommunicatingAvatarMind extends KeyboardAvatarMind<BufferedReader> {

	private int response = 0;

	public CommunicatingAvatarMind(InputStream input) {
		super(new BufferedReader(new InputStreamReader(input)));
		this.componentmap.put(CommunicationAction.class, SimpleComponentKey.ACTUATOR);
		this.actionmap.put(CommunicativeResponse.ACTIONKEY, new CommunicationActionFactory());
	}

	public CommunicatingAvatarMind() {
		super(new BufferedReader(new InputStreamReader(System.in)));
		this.componentmap.put(CommunicationAction.class, SimpleComponentKey.ACTUATOR);
		this.actionmap.put(CommunicativeResponse.ACTIONKEY, new CommunicationActionFactory());
	}

	@Override
	public void cycle() {
		super.cycle();
		response = 0;
	}

	@Override
	public UserResponse<String> getUserResponse() {
		if (response == 0) {
			try {
				response++;
				return new CommunicativeResponse(this.reader.readLine());
			} catch (IOException e) {
				throw new StarWorldsRuntimeException("Failed to get user input", e);
			}
		} else {
			return null;
		}

	}
}
