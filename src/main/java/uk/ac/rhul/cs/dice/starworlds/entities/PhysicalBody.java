package uk.ac.rhul.cs.dice.starworlds.entities;

import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.PhysicalBodyAppearance;

/**
 * The most generic class for physical bodies implementing {@link Body}. It extends {@link CustomObservable} and can
 * become a {@link CustomObserver}. It has a {@link String} id and an {@link Appearance}.<br/>
 * <br/>
 * 
 * Known direct subclasses: {@link ActiveBody}, {@link PassiveBody}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class PhysicalBody extends AbstractManifest implements Body {

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            : the unique identifier of this {@link PhysicalBody}.
	 * @param appearance
	 *            : the {@link Appearance} of this {@link PhysicalBody}.
	 */
	public PhysicalBody(String id, PhysicalBodyAppearance appearance) {
		super(id, appearance);
	}

	@Override
	public final void setId(String id) {
		throw new UnsupportedOperationException("Cannot reset the id of " + this.getClass().getSimpleName());
	}
}