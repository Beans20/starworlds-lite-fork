package uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset;

import java.util.Collection;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter.FilterKey;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;
import uk.ac.rhul.cs.dice.starworlds.perception.factory.PerceptionFactory;

public class SenseActionRuleSet<A extends SenseAction, E extends Environment<E>, P extends Perception> implements
		ActionRuleSet<A, E> {

	private PerceptionFactory<P> factory;

	public SenseActionRuleSet(PerceptionFactory<P> factory) {
		this.factory = factory;
	}

	@Override
	public boolean isPossible(A action, E environment) {
		// System.out.println("isPossible: " + action);
		// TODO check that the sensor can actually perceive what it is requesting ?
		if (environment.getState().getAttributeKeys().contains(action.getAttribute())) {
			Collection<FilterKey<?, ?>> filters = environment.getState().getFilterKeys();
			for (FilterKey<?, ?> key : action.getFilterKeys()) {
				if (!filters.contains(key)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean perform(A action, E environment) {
		// apply filters
		Object result = action.getChain().perform(environment.getState(), action);
		// construct perception
		P perception = this.factory.getPerception(result);
		// get and notify the sensor that attempted the action,
		// possibly this will be moved to a utility function in the subscriber?
		environment.notifySensors(perception, action);
		return true;
	}

	@Override
	public boolean verify(A action, E environment) {
		return true;
	}

}
