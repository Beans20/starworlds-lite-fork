package uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete;

import uk.ac.rhul.cs.dice.starworlds.annotations.SensiblePerception;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.AbstractPassiveSensor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;
import uk.ac.rhul.cs.dice.starworlds.perception.CommunicationPerception;

/**
 * A general concrete {@link AbstractPassiveSensor} that can sense {@link CommunicationPerception}s.
 * 
 * @author Ben Wilkins
 *
 * @param <K>
 *            {@link ComponentKey} type.
 */
@SensiblePerception({ CommunicationPerception.class })
public class GeneralPassiveSensor<K extends ComponentKey> extends AbstractPassiveSensor<K> {

	public GeneralPassiveSensor(K componentKey) {
		super(componentKey);
	}

}
