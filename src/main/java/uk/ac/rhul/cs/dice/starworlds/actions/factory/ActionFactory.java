package uk.ac.rhul.cs.dice.starworlds.actions.factory;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;

public interface ActionFactory<A extends EnvironmentalAction> {

	public A create(Object[] args);

}
