package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key;

import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.Key;

public abstract class AttributeKey<V> extends Key {

	private Class<? extends V> attributeClass;

	public AttributeKey(Class<? extends V> attributeClass) {
		super();
		this.attributeClass = attributeClass;
	}

	public Class<? extends V> getAttributeClass() {
		return attributeClass;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return this.attributeClass.getSimpleName();
	}

}
