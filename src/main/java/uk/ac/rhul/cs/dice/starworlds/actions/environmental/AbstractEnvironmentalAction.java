package uk.ac.rhul.cs.dice.starworlds.actions.environmental;

import uk.ac.rhul.cs.dice.starworlds.actions.Action;
import uk.ac.rhul.cs.dice.starworlds.appearances.ActiveBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.EnvironmentAppearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.Physics;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset.ActionRuleSet;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset.CommunicationActionRuleSet;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset.SenseActionRuleSet;
import uk.ac.rhul.cs.dice.starworlds.event.AbstractEvent;
import uk.ac.rhul.cs.dice.starworlds.initialisation.IDFactory;

// TODO: Auto-generated Javadoc
/**
 * The abstract class implementing {@link EnvironmentalAction}.<br>
 * Every action should have an associated {@link ActionRuleSet} which defines how the action is executed, as well as pre
 * and post conditions. Examples of these for {@link SenseAction} and {@link CommunicationAction}s are given
 * {@link SenseActionRuleSet} and {@link CommunicationActionRuleSet} respectively. <br/>
 * {@link ActionRuleSet}s must be added to a {@link Physics}s before simulation.
 * 
 * Known direct subclasses: {@link PhysicalAction}, {@link CommunicationAction}, {@link SenseAction}.
 * 
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class AbstractEnvironmentalAction extends AbstractEvent implements EnvironmentalAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4577974925124248710L;

	/** The id of the action. */
	private String id;

	/** The actor who attempted this {@link Action}. */
	private ActiveBodyAppearance actor;

	/** The local environment that the {@link Action} is attempted in. */
	private EnvironmentAppearance localEnvironment;

	/**
	 * The default constructor.
	 */
	public AbstractEnvironmentalAction() {
		this.id = IDFactory.getInstance().getNewID();
	}

	/**
	 * Instantiates a new abstract environmental action.
	 *
	 * @param actor
	 *            the actor
	 */
	public AbstractEnvironmentalAction(ActiveBodyAppearance actor) {
		this.actor = actor;
		this.id = IDFactory.getInstance().getNewID();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see uk.ac.rhul.cs.dice.starworlds.actions.Action#getActor()
	 */
	@Override
	public ActiveBodyAppearance getActor() {
		return this.actor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * uk.ac.rhul.cs.dice.starworlds.actions.Action#setActor(uk.ac.rhul.cs.dice.starworlds.appearances.ActiveBodyAppearance
	 * )
	 */
	@Override
	public void setActor(ActiveBodyAppearance actor) {
		this.actor = actor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see uk.ac.rhul.cs.dice.starworlds.utils.Identifiable#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/**
	 * The ID of an {@link Action} cannot be set after instantiationO.
	 */
	@Override
	public final void setId(String id) {
		// the id of an action cannot be changed
	}

	/**
	 * Gets the local environment.
	 *
	 * @return the local environment
	 */
	public EnvironmentAppearance getLocalEnvironment() {
		return this.localEnvironment;
	}

	/**
	 * Sets the local environment.
	 *
	 * @param localEnvironment
	 *            the new local environment
	 */
	public void setLocalEnvironment(EnvironmentAppearance localEnvironment) {
		this.localEnvironment = localEnvironment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see uk.ac.rhul.cs.dice.starworlds.event.Event#getOrigin()
	 */
	@Override
	public Identifiable getOrigin() {
		return this.getActor();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ":" + this.id;
	}
}