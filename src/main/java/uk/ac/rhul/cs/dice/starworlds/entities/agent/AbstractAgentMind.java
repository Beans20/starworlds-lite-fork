package uk.ac.rhul.cs.dice.starworlds.entities.agent;

import uk.ac.rhul.cs.dice.starworlds.actions.Action;
import uk.ac.rhul.cs.dice.starworlds.entities.Agent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Sensor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.concrete.SimpleMind;

/**
 * 
 * 
 * 
 * </br> Known direct subclasses: {@link SimpleMind}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class AbstractAgentMind<K extends ComponentKey> extends AbstractMind<K> {

	public AbstractAgentMind() {
	}

	@Override
	public void cycle() {
		this.perceive();
		this.decide();
		this.execute();
	}

	/**
	 * The perceive procedure. Any perceptions received by this {@link Agent}s {@link Sensor}s should be processed here.
	 * 
	 */
	public abstract void perceive();

	/**
	 * 
	 * The decide procedure. The agent should come to a decision about what {@link Action}s it would like to attempt.
	 * 
	 * 
	 */
	public abstract void decide();

	/**
	 * The execute procedure. The agent should process the actions it has decided on and arrive at the action it would
	 * like to attempt.
	 * 
	 * @param parameters
	 *            an array of optional parameters that may be used to select the action to attempt.
	 * @return
	 */
	public abstract void execute();
}