package uk.ac.rhul.cs.dice.starworlds.environment.physics.actiondefinition;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset.CommunicationActionRuleSet;

public class CommunicationActionDefinition<E extends Environment<E>> extends ActionDefinition<CommunicationAction, E> {

	public CommunicationActionDefinition() {
		super(CommunicationAction.class, new CommunicationActionRuleSet<CommunicationAction, E>());
	}

}
