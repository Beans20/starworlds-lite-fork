package uk.ac.rhul.cs.dice.starworlds.test;

import java.util.HashSet;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.PassiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgentMind;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimpleComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.concrete.SimpleAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.concrete.CommunicatingAvatarMind;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.concrete.SimpleAvatar;

public class AvatarCommunicationTest extends TestSuite {

	public static void main(String[] args) {
		AvatarCommunicationTest test = new AvatarCommunicationTest();
		test.physics.setFramelength(0);
		test.physics.infoOn();
		test.simulate();
	}

	@Override
	public Set<AbstractAgent<?>> getAgents() {
		Set<AbstractAgent<?>> agents = new HashSet<>();
		agents.add(new SimpleAgent(new AgentTestMind()));
		return agents;
	}

	@Override
	public Set<ActiveBody<?>> getActiveBodies() {
		return null;
	}

	@Override
	public Set<PassiveBody> getPassiveBodies() {
		return null;
	}

	@Override
	public Set<AbstractAvatarAgent<?>> getAvatars() {
		Set<AbstractAvatarAgent<?>> avatars = new HashSet<>();
		avatars.add(new SimpleAvatar(new CommunicatingAvatarMind()));
		return avatars;
	}

	private class AgentTestMind extends AbstractAgentMind<SimpleComponentKey> {

		@Override
		public void perceive() {
			System.out.println("Agent:" + this.getId() + " percieves: " + this.getBody().perceive());
		}

		@Override
		public void decide() {
			// pass
		}

		@Override
		public void execute() {
			// pass
		}

	}
}
