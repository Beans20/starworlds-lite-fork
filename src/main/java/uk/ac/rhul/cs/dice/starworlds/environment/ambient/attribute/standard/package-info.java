/**
 * The classes in this package are the default {@link uk.ac.rhul.cs.dice.starworlds.environment.ambient.Ambient Ambient} {@link uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.Attribute Attributes}.
 */
package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard;