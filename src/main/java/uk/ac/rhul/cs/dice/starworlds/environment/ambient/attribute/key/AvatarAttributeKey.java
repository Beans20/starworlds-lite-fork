package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key;

import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;

public class AvatarAttributeKey extends AttributeKey<MapAttribute<String, AbstractAvatarAgent<?>>> {

	public AvatarAttributeKey(Class<MapAttribute<String, AbstractAvatarAgent<?>>> cls) {
		super(cls);
	}

}
