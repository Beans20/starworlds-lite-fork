package uk.ac.rhul.cs.dice.starworlds.entities.avatar;

public interface UserResponse<G> {

	public Object[] getResponse();

	public G getKey();

}
