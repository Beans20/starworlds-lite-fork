package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard;

import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgent;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;

public class AgentsAttribute extends MapAttribute<String, AbstractAgent<?>> {

	public AgentsAttribute(Map<String, AbstractAgent<?>> attribute) {
		super(attribute);
	}

	@Override
	public String toString() {
		return new StringBuilder("Agents: ").append(System.lineSeparator()).append(super.toString("  ")).toString();
	}
}
