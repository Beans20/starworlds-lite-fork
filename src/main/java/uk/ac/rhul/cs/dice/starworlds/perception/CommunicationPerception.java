package uk.ac.rhul.cs.dice.starworlds.perception;

import java.io.Serializable;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;

/**
 * A {@link Perception} representing the result of a {@link CommunicationAction}. It contains a {@link Serializable}
 * message and the senders id.
 * 
 * @author Ben Wilkins
 *
 */
public class CommunicationPerception extends AbstractPerception {

	private static final long serialVersionUID = 2576899013501867189L;
	private Serializable message;

	public CommunicationPerception(Serializable message) {
		this.message = message;
	}

	public Serializable getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "message: " + message;
	}
}
