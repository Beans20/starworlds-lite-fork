package uk.ac.rhul.cs.dice.starworlds.appearances;

import uk.ac.rhul.cs.dice.starworlds.entities.Entity;
import uk.ac.rhul.cs.dice.starworlds.entities.Manifest;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;

/**
 * The {@link Appearance} of some {@link Entity} or {@link Environment}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class AbstractAppearance implements Appearance {

	private transient Manifest manifest = null;

	/**
	 * Constructor.
	 */
	public AbstractAppearance() {
		manifest = null;
	}

	/**
	 * Constructor.
	 * 
	 * @param manifest
	 *            : the {@link Manifest} that this is {@link Appearance} of
	 */
	public AbstractAppearance(Manifest manifest) {
		this.manifest = manifest;
	}

	@Override
	public void manifest(Manifest manifest) {
		if (this.manifest == null) {
			this.manifest = manifest;
		} else {
			throw new StarWorldsRuntimeException("Cannot manifest an appearance more than once.");
		}

	}

	protected Manifest getManifest() {
		return manifest;
	}

	@Override
	public final String getId() {
		return manifest.getId();
	}

	@Override
	public final void setId(String id) {
		throw new UnsupportedOperationException("The id of an Appearance cannot be set");
	}

	@Override
	public String represent() {
		return manifest.getClass().getSimpleName();
	}

	@Override
	public String toString() {
		return represent();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((manifest == null) ? 0 : manifest.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractAppearance other = (AbstractAppearance) obj;
		if (manifest == null) {
			if (other.manifest != null)
				return false;
		} else if (!manifest.equals(other.manifest))
			return false;
		return true;
	}

}