package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard;

import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;

public class ActiveBodiesAttribute extends MapAttribute<String, ActiveBody<?>> {
	public ActiveBodiesAttribute(Map<String, ActiveBody<?>> attribute) {
		super(attribute);
	}

	@Override
	public String toString() {
		return new StringBuilder("ActiveBodies: ").append(System.lineSeparator()).append(super.toString("  "))
				.toString();
	}
}
