package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute;

import java.util.UUID;

public abstract class Key {

	protected UUID key;

	public Key() {
		key = UUID.randomUUID();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + key.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!getClass().isAssignableFrom(obj.getClass()))
			return false;
		Key other = (Key) obj;
		return this.key.equals(other.key);
	}

}
