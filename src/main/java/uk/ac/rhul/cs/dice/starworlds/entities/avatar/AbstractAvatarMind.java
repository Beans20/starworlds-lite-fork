package uk.ac.rhul.cs.dice.starworlds.entities.avatar;

import java.util.HashMap;
import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.factory.ActionFactory;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractMind;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;

public abstract class AbstractAvatarMind<G, K extends ComponentKey> extends AbstractMind<K> {

	protected Map<G, ActionFactory<? extends EnvironmentalAction>> actionmap;
	protected Map<Class<?>, K> componentmap;

	public AbstractAvatarMind() {
		actionmap = new HashMap<>();
		componentmap = new HashMap<>();
	}
}
