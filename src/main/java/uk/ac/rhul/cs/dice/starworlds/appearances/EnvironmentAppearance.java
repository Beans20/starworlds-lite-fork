package uk.ac.rhul.cs.dice.starworlds.appearances;

import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializableAppearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializableEnvironmentAppearance;
import uk.ac.rhul.cs.dice.starworlds.environment.AbstractEnvironment;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;

public class EnvironmentAppearance extends AbstractAppearance {

	public EnvironmentAppearance() {
		super();
	}

	@Override
	public String toString() {
		return Environment.class.getSimpleName() + ":" + this.getId();
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public SerializableAppearance getSerializable() {
		return new SerializableEnvironmentAppearance((AbstractEnvironment<?>) this.getManifest());
	}
}