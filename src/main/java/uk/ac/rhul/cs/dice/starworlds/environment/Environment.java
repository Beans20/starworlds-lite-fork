package uk.ac.rhul.cs.dice.starworlds.environment;

import java.util.Collection;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.PhysicalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.entities.Manifest;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.Ambient;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.Physics;
import uk.ac.rhul.cs.dice.starworlds.environment.subscription.SensorSubscriptionHandler;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

/**
 * The most general interface exposing the essential methods all the environments should to implement.<br/>
 * <br/>
 * 
 * Known implementations: {@link AbstractEnvironment}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface Environment<E extends Environment<E>> extends Manifest {

	public void postInitialisation();

	public void notifySensors(Perception perception, Collection<String> agents);

	public void notifySensors(Perception perception, String agent);

	public void notifySensors(Perception perception);

	public void notifySensors(Perception perception, SenseAction action);

	/**
	 * Returns the {@link Ambient} of the {@link Environment}.
	 * 
	 * @return the {@link Ambient} of the {@link Environment}.
	 */
	public Ambient getState();

	/**
	 * Sets the {@link Ambient} of the {@link Environment}.
	 * 
	 * @param state
	 *            : the {@link Ambient} of the {@link Environment}.
	 */
	public void setState(Ambient state);

	/**
	 * Returns the {@link Physics} of the {@link Environment}.
	 * 
	 * @return the {@link Physics} of the {@link Environment}.
	 */
	public Physics<E> getPhysics();

	public SensorSubscriptionHandler getSubscriptionHandler();

	/**
	 * Sets the {@link Physics} of the {@link Environment}.
	 * 
	 * @param physics
	 *            : the {@link Physics} of the {@link Environment}.
	 */
	public void setPhysics(Physics<E> physics);

	/**
	 * Returns the {@link Appearance} of the {@link Environment}.
	 * 
	 * @return the {@link Appearance} of the {@link Environment}.
	 */
	public Appearance getAppearance();

	/**
	 * Checks whether the {@link Environment} is simple or not.
	 * 
	 * @return a {@link Boolean} value representing whether the {@link Environment} is simple or not.
	 */
	public Boolean isSimple();

	// TODO possibly remove the above?

	public void attemptSenseAction(SenseAction action);

	public void attemptPhysicalAction(PhysicalAction action);

	public void attemptCommunicationAction(CommunicationAction action);

	// ****

	public void attemptAction(EnvironmentalAction action);

	public Collection<SenseAction> flushSensingActions();

	public Collection<PhysicalAction> flushPhysicalActions();

	public Collection<CommunicationAction> flushCommunicationActions();

	public Collection<SenseAction> getSensingActions();

	public Collection<PhysicalAction> getPhysicalActions();

	public Collection<CommunicationAction> getCommunicationActions();
}