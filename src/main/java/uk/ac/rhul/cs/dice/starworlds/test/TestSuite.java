package uk.ac.rhul.cs.dice.starworlds.test;

import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.PassiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.environment.concrete.SimpleAmbient;
import uk.ac.rhul.cs.dice.starworlds.environment.concrete.SimplePhysics;
import uk.ac.rhul.cs.dice.starworlds.environment.concrete.SimpleUniverse;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.actiondefinition.CommunicationActionDefinition;
import uk.ac.rhul.cs.dice.starworlds.environment.subscription.SensorSubscriptionHandler;

public abstract class TestSuite {

	SimplePhysics physics;
	SimpleUniverse universe;
	SimpleAmbient ambient;

	public TestSuite() {
		physics = new SimplePhysics();
		ambient = new SimpleAmbient(getAgents(), getActiveBodies(), getPassiveBodies(), getAvatars());
		universe = new SimpleUniverse(ambient, physics, new SensorSubscriptionHandler());
		CommunicationActionDefinition<SimpleUniverse> communicationAD = new CommunicationActionDefinition<SimpleUniverse>();
		physics.addActionDefinition(communicationAD);
	}

	public void simulate() {
		universe.simulate();
	}

	public abstract Set<AbstractAgent<?>> getAgents();

	public abstract Set<ActiveBody<?>> getActiveBodies();

	public abstract Set<PassiveBody> getPassiveBodies();

	public abstract Set<AbstractAvatarAgent<?>> getAvatars();

}
