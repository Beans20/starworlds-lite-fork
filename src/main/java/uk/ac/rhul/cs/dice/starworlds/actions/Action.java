package uk.ac.rhul.cs.dice.starworlds.actions;

import uk.ac.rhul.cs.dice.starworlds.appearances.ActiveBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.Actor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Actuator;
import uk.ac.rhul.cs.dice.starworlds.event.Event;

/**
 * The interface for all actions.<br>
 * An action is an {@link Event} that an {@link ActiveBody} can attempt using a suitable {@link Actuator}.
 * 
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface Action extends Event {

	/**
	 * Returns the {@link ActiveBodyAppearance} of the {@link Actor} that performed this {@link Action}
	 * 
	 * @return the {@link ActiveBodyAppearance}.
	 */
	public abstract ActiveBodyAppearance getActor();

	/**
	 * Sets the {@link ActiveBodyAppearance} of the {@link Actor} that performed this {@link Action}.
	 * 
	 * @param actor
	 *            : the {@link ActiveBodyAppearance} to be set.
	 */
	public abstract void setActor(ActiveBodyAppearance actor);
}