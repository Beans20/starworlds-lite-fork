package uk.ac.rhul.cs.dice.starworlds.environment.ambient;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.PassiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.Attribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.AttributeMap;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key.AttributeKey;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.ActiveBodiesAttribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.AgentsAttribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.AvatarsAttribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.PassiveBodiesAttribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter.FilterKey;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AbstractPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.Portal;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.processes.Process;

public class AbstractAmbient implements Ambient {

	public static final AttributeKey<MapAttribute<?, ?>> AGENTSKEY = new AttributeKey<MapAttribute<?, ?>>(
			AgentsAttribute.class) {
	};
	public static final AttributeKey<MapAttribute<?, ?>> AVATARSKEY = new AttributeKey<MapAttribute<?, ?>>(
			AvatarsAttribute.class) {
	};
	public static final AttributeKey<MapAttribute<?, ?>> ACTIVEBODIESKEY = new AttributeKey<MapAttribute<?, ?>>(
			ActiveBodiesAttribute.class) {
	};
	public static final AttributeKey<MapAttribute<?, ?>> PASSIVEBODIESKEY = new AttributeKey<MapAttribute<?, ?>>(
			PassiveBodiesAttribute.class) {
	};

	private AttributeMap attributes;
	private Set<FilterKey<?, ?>> filters;

	private Set<Class<? extends EnvironmentalAction>> actionDefinitions;

	protected Map<String, Process> processes; // TODO
	protected Map<String, AbstractPortal> portals; // TODO

	protected Map<String, AbstractAvatarAgent<?>> avatars;
	protected Map<String, AbstractAgent<?>> agents;
	protected Map<String, ActiveBody<?>> activeBodies;
	protected Map<String, PassiveBody> passiveBodies;

	public AbstractAmbient(Set<AbstractAgent<?>> agents, Set<ActiveBody<?>> activeBodies, Set<PassiveBody> passiveBodies) {
		init(agents, activeBodies, passiveBodies, null, null, null);
	}

	public AbstractAmbient(Set<AbstractAgent<?>> agents, Set<ActiveBody<?>> activeBodies,
			Set<PassiveBody> passiveBodies, Set<AbstractAvatarAgent<?>> avatars) {
		init(agents, activeBodies, passiveBodies, avatars, null, null);
	}

	public AbstractAmbient(Set<AbstractAgent<?>> agents, Set<ActiveBody<?>> activeBodies,
			Set<PassiveBody> passiveBodies, Set<AbstractAvatarAgent<?>> avatars, Set<AbstractPortal> portals) {
		init(agents, activeBodies, passiveBodies, avatars, portals, null);
	}

	protected void init(Set<AbstractAgent<?>> agents, Set<ActiveBody<?>> activeBodies, Set<PassiveBody> passiveBodies,
			Set<AbstractAvatarAgent<?>> avatars, Set<AbstractPortal> portals, Set<Process> processes) {
		this.avatars = (avatars != null) ? setToMap(avatars) : new HashMap<>();
		this.agents = (agents != null) ? setToMap(agents) : new HashMap<>();
		this.activeBodies = (activeBodies != null) ? setToMap(activeBodies) : new HashMap<>();
		this.passiveBodies = (passiveBodies != null) ? setToMap(passiveBodies) : new HashMap<>();
		this.portals = (portals != null) ? setToMap(portals) : new HashMap<>();
		// this.processes = (processes != null) ? setToMap(portals) : new HashMap<>(); TODO
		this.filters = new HashSet<>();
		this.attributes = new AttributeMap();
		initAttributes();
	}

	protected void initAttributes() {
		addAttibute(AGENTSKEY, new AgentsAttribute(this.agents));
		addAttibute(AVATARSKEY, new AvatarsAttribute(this.avatars));
		addAttibute(ACTIVEBODIESKEY, new ActiveBodiesAttribute(this.activeBodies));
		addAttibute(PASSIVEBODIESKEY, new PassiveBodiesAttribute(this.passiveBodies));
	}

	@Override
	public <V extends Attribute> void addAttibute(AttributeKey<V> key, V attribute) {
		this.attributes.put(key, attribute);
	}

	@Override
	public <V extends Attribute> V getAttribute(AttributeKey<V> key) {
		return this.attributes.get(key);
	}

	@Override
	public Collection<AttributeKey<?>> getAttributeKeys() {
		return this.attributes.keySet();
	}

	@Override
	public Collection<Attribute> getAttributes() {
		return this.attributes.values();
	}

	@Override
	public boolean attributeExists(AttributeKey<?> key) {
		return this.attributes.containsKey(key);
	}

	@Override
	public <A, R> void addFilter(FilterKey<A, R> key) {
		this.filters.add(key);
	}

	@Override
	public boolean filterExists(FilterKey<?, ?> key) {
		return this.filters.contains(key);
	}

	@Override
	public Collection<FilterKey<?, ?>> getFilterKeys() {
		return filters;
	}

	@Override
	public boolean actionDefined(Class<? extends EnvironmentalAction> action) {
		return actionDefinitions.contains(action);
	}

	@Override
	public void addActionDefinition(Class<? extends EnvironmentalAction> definition) {
		if (!definition.isInterface() || !Modifier.isAbstract(definition.getModifiers())) {
			this.actionDefinitions.add(definition);
		}
	}

	@Override
	public void addActionDefinitions(Collection<Class<? extends EnvironmentalAction>> definitions) {
		for (Class<? extends EnvironmentalAction> def : definitions) {
			addActionDefinition(def);
		}
	}

	@Override
	public Set<Class<? extends EnvironmentalAction>> getActionDefinitions() {
		return actionDefinitions;
	}

	@Override
	public AbstractAgent<?> getAgent(String id) {
		return this.agents.get(id);
	}

	@Override
	public ActiveBody<?> getActiveBody(String id) {
		return this.activeBodies.get(id);
	}

	@Override
	public PassiveBody getPassiveBody(String id) {
		return this.passiveBodies.get(id);
	}

	@Override
	public AbstractAvatarAgent<?> getAvatar(String id) {
		return this.avatars.get(id);
	}

	@Override
	public void addAvatar(AbstractAvatarAgent<?> avatar) {
		this.avatars.put(avatar.getId(), avatar);
	}

	@Override
	public void addAgent(AbstractAgent<?> agent) {
		this.agents.put(agent.getId(), agent);
	}

	@Override
	public void addActiveBody(ActiveBody<?> body) {
		this.activeBodies.put(body.getId(), body);
	}

	@Override
	public void addPassiveBody(PassiveBody body) {
		this.passiveBodies.put(body.getId(), body);
	}

	@Override
	public void addPortal(AbstractPortal portal) {
		this.portals.put(portal.getId(), portal);
	}

	@Override
	public Collection<AbstractAvatarAgent<?>> getAvatars() {
		return this.avatars.values();
	}

	@Override
	public Collection<AbstractAgent<?>> getAgents() {
		return this.agents.values();
	}

	@Override
	public Collection<ActiveBody<?>> getActiveBodies() {
		return this.activeBodies.values();
	}

	@Override
	public Collection<PassiveBody> getPassiveBodies() {
		return this.passiveBodies.values();
	}

	private <T extends Identifiable> Map<String, T> setToMap(Set<T> set) {
		Map<String, T> map = new HashMap<>();
		set.forEach((T t) -> {
			;
			map.put((String) t.getId(), t);
		});
		return map;
	}

	@Override
	public Collection<AbstractPortal> getPortals() {
		return portals.values();
	}

	@Override
	public Collection<Process> getProcesses() {
		return processes.values();
	}

	@Override
	public Portal getPortal(String id) {
		return portals.get(id);
	}

	@Override
	public Process getProcess(String id) {
		return processes.get(id);
	}

}
