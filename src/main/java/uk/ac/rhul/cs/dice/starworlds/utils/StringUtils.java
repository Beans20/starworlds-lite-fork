package uk.ac.rhul.cs.dice.starworlds.utils;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

public class StringUtils {

	public static String arrayToString(boolean[][] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append("[");
			for (int j = 0; j < array[i].length - 1; j++) {
				builder.append(array[i][j]).append(",");
			}
			builder.append(array[i][array[i].length - 1]).append("],").append(System.lineSeparator()).append(" ");
		}
		builder.append("[");
		for (int j = 0; j < array[array.length - 1].length - 1; j++) {
			builder.append(array[array.length - 1][j]).append(",");
		}
		builder.append(array[array.length - 1][array[array.length - 1].length - 1]).append("]]");
		return builder.toString();
	}

	public static String arrayToString(boolean[] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append(array[i]).append(",");
		}
		builder.append(array[array.length - 1]).append("]");
		return builder.toString();
	}

	public static String arrayToString(int[][] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append("[");
			for (int j = 0; j < array[i].length - 1; j++) {
				builder.append(array[i][j]).append(",");
			}
			builder.append(array[i][array[i].length - 1]).append("],").append(System.lineSeparator()).append(" ");
		}
		builder.append("[");
		for (int j = 0; j < array[array.length - 1].length - 1; j++) {
			builder.append(array[array.length - 1][j]).append(",");
		}
		builder.append(array[array.length - 1][array[array.length - 1].length - 1]).append("]]");
		return builder.toString();
	}

	public static String arrayToString(int[] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append(array[i]).append(",");
		}
		builder.append(array[array.length - 1]).append("]");
		return builder.toString();
	}

	public static String arrayToString(char[][] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append("[");
			for (int j = 0; j < array[i].length - 1; j++) {
				builder.append(array[i][j]).append(",");
			}
			builder.append(array[i][array[i].length - 1]).append("],").append(System.lineSeparator()).append(" ");
		}
		builder.append("[");
		for (int j = 0; j < array[array.length - 1].length - 1; j++) {
			builder.append(array[array.length - 1][j]).append(",");
		}
		builder.append(array[array.length - 1][array[array.length - 1].length - 1]).append("]]");
		return builder.toString();
	}

	public static String arrayToString(char[] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append(array[i]).append(",");
		}
		builder.append(array[array.length - 1]).append("]");
		return builder.toString();
	}

	public static String arrayToString(float[][] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append("[");
			for (int j = 0; j < array[i].length - 1; j++) {
				builder.append(array[i][j]).append(",");
			}
			builder.append(array[i][array[i].length - 1]).append("],").append(System.lineSeparator()).append(" ");
		}
		builder.append("[");
		for (int j = 0; j < array[array.length - 1].length - 1; j++) {
			builder.append(array[array.length - 1][j]).append(",");
		}
		builder.append(array[array.length - 1][array[array.length - 1].length - 1]).append("]]");
		return builder.toString();
	}

	public static String arrayToString(float[] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append(array[i]).append(",");
		}
		builder.append(array[array.length - 1]).append("]");
		return builder.toString();
	}

	public static String arrayToString(double[][] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append("[");
			for (int j = 0; j < array[i].length - 1; j++) {
				builder.append(array[i][j]).append(",");
			}
			builder.append(array[i][array[i].length - 1]).append("],").append(System.lineSeparator()).append(" ");
		}
		builder.append("[");
		for (int j = 0; j < array[array.length - 1].length - 1; j++) {
			builder.append(array[array.length - 1][j]).append(",");
		}
		builder.append(array[array.length - 1][array[array.length - 1].length - 1]).append("]]");
		return builder.toString();
	}

	public static String arrayToString(double[] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append(array[i]).append(",");
		}
		builder.append(array[array.length - 1]).append("]");
		return builder.toString();
	}

	public static <T> String arrayToString(T[][] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append("[");
			for (int j = 0; j < array[i].length - 1; j++) {
				builder.append(array[i][j]).append(",");
			}
			builder.append(array[i][array[i].length - 1]).append("],").append(System.lineSeparator()).append(" ");
		}
		builder.append("[");
		for (int j = 0; j < array[array.length - 1].length - 1; j++) {
			builder.append(array[array.length - 1][j]).append(",");
		}
		builder.append(array[array.length - 1][array[array.length - 1].length - 1]).append("]]");
		return builder.toString();
	}

	public static <T> String arrayToString(T[] array) {
		StringBuilder builder = new StringBuilder("[");
		for (int i = 0; i < array.length - 1; i++) {
			builder.append(array[i]).append(",");
		}
		builder.append(array[array.length - 1]).append("]");
		return builder.toString();
	}

	public static String mapToString(Map<?, ?> map, String indent) {
		StringBuilder b = new StringBuilder();
		map.forEach((k, v) -> b.append(indent).append(k.toString()).append("->").append(v.toString())
				.append(System.lineSeparator()));
		return b.toString();
	}

	public static String mapToString(Map<?, ?> map) {
		StringBuilder b = new StringBuilder();
		map.forEach((k, v) -> b.append(k.toString()).append("->").append(v.toString()).append(System.lineSeparator()));
		return b.toString();
	}

	public static String collectionToString(Collection<?> col) {
		StringBuilder b = new StringBuilder();
		col.forEach(v -> b.append(v.toString()).append(System.lineSeparator()));
		return b.toString();
	}

	public static String collectionToString(Collection<?> col, String indent) {
		StringBuilder b = new StringBuilder();
		col.forEach(v -> b.append(indent).append(v.toString()).append(System.lineSeparator()));
		return b.toString();
	}

	public static <V> String collectionToStringSingle(Collection<V> col, Function<V, String> toString) {
		StringBuilder b = new StringBuilder("{");
		col.forEach(v -> b.append(toString.apply(v)).append(","));
		if (b.length() > 1) {
			b.deleteCharAt(b.length() - 1);
		}
		b.append("}");
		return b.toString();
	}

	public static <V> String collectionToStringSingle(Collection<V> col) {
		StringBuilder b = new StringBuilder("{");
		col.forEach(v -> b.append(v).append(","));
		if (b.length() > 1) {
			b.deleteCharAt(b.length() - 1);
		}
		b.append("}");
		return b.toString();
	}

}
