package uk.ac.rhul.cs.dice.starworlds.perception.factory;

import uk.ac.rhul.cs.dice.starworlds.perception.ActivePerception;

public class ActivePerceptionFactory implements PerceptionFactory<ActivePerception> {

	@Override
	public ActivePerception getPerception(Object... args) {
		return new ActivePerception();
	}

}
