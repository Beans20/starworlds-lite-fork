package uk.ac.rhul.cs.dice.starworlds.perception;

import uk.ac.rhul.cs.dice.starworlds.event.Event;

/**
 * The interface for perceptions.<br/>
 * <br/>
 * 
 * Known implementations: {@link AbstractPerception}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface Perception extends Event {

}