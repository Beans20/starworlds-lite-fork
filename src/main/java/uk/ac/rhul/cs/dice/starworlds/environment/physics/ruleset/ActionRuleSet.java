package uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;

public interface ActionRuleSet<A extends EnvironmentalAction, E extends Environment<E>> {

	public boolean perform(A action, E environment);

	public boolean isPossible(A action, E environment);

	public boolean verify(A action, E environment);

}
