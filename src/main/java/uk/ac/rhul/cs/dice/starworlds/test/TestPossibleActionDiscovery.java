package uk.ac.rhul.cs.dice.starworlds.test;

import java.util.Collection;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.PhysicalAction;
import uk.ac.rhul.cs.dice.starworlds.annotations.PossibleActionDiscovery;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.GeneralActuator;
import uk.ac.rhul.cs.dice.starworlds.utils.StringUtils;

public class TestPossibleActionDiscovery {

	public static void main(String[] args) throws ClassNotFoundException {
		System.out.println(StringUtils.mapToString(PossibleActionDiscovery.getPossibleActions()));
		System.out.println(StringUtils.collectionToString(PossibleActionDiscovery
				.getPossibleActions(GeneralActuator.class)));
		Collection<Class<? extends EnvironmentalAction>> col = PossibleActionDiscovery.getPossibleActions(GeneralActuator.class);
		System.out.println(col.contains(PhysicalAction.class));
	}
}
