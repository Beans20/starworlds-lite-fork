package uk.ac.rhul.cs.dice.starworlds.environment.subscription;

import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import uk.ac.rhul.cs.dice.starworlds.annotations.Discovery;
import uk.ac.rhul.cs.dice.starworlds.annotations.SensiblePerception;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.AbstractActiveSensor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.AbstractPassiveSensor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Sensor;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

public class SensorSubscriptionHandler implements SubcriptionHandler<Sensor<?>, Class<? extends Perception>> {

	public static final Class<SensiblePerception> SENSEANNOTATION = SensiblePerception.class;

	/**
	 * A {@link SensorPerceptionMap} describing the which {@link Sensor}s classes subscribe to which {@link Perception}
	 * classes.
	 */
	private SensiblePerceptionMap sensorMap;
	/**
	 * A {@link Map} describing which instances of {@link Sensor} (and their {@link ActiveBody}) subcribe to which
	 * {@link Perception} classes.
	 */
	private Map<Class<?>, Map<String, List<Sensor<?>>>> perceptionMap;

	public SensorSubscriptionHandler() {
		ScanResult result;
		if (Discovery.scanned()) {
			result = Discovery.scan();
		} else {
			result = Discovery.newScan(new String[] {});
		}
		Set<Class<?>> sensors = result.getNamesOfClassesImplementing(Sensor.class).stream().map(this::getSensorClass)
				.collect(Collectors.toSet());
		System.out.println(sensors);
		init(sensors);
	}

	public SensorSubscriptionHandler(Set<Class<?>> sensors) {
		init(sensors);
	}

	private void init(Collection<Class<?>> sensors) {
		sensorMap = new SensiblePerceptionMap(sensors);
		perceptionMap = new HashMap<>();
		sensorMap.getValues().forEach(s -> perceptionMap.put(s, new HashMap<>()));
	}

	private Class<?> getSensorClass(String name) {
		try {
			return Class.forName(name);
		} catch (ClassNotFoundException e) {
			throw new StarWorldsRuntimeException("Failed to initialise: " + this.getClass().getSimpleName()
					+ " try using the alternative constructor.");
		}
	}

	/**
	 * Get all the {@link Sensor}s who subscribe to the given class of {@link Perception}s.
	 * 
	 * @param pcls
	 *            : class of {@link Perception}
	 * @return A {@link Map} containing the {@link ActiveBody}s id as a key and a list of its {@link Sensor} who have
	 *         subscribed to the given {@link Perception}.
	 */
	public Map<String, List<Sensor<?>>> getSensors(Class<? extends Perception> perceptionclass) {
		return perceptionMap.get(perceptionclass);
	}

	public void subscribeActiveBody(ActiveBody<?> body) {
		System.out.println("subscribe body: " + body);
		try {
			body.getSensors().stream().collect(Collectors.groupingBy(Object::getClass))
					.forEach((c, l) -> sensorMap.get(c).forEach(p -> perceptionMap.get(p).put(body.getId(), l)));
		} catch (Exception e) {
			throw new StarWorldsRuntimeException(System.lineSeparator() + "Failed to subscribe ActiveBody: " + body
					+ System.lineSeparator()
					+ "perhaps you forgot to add some Sensor Class to the SubscriptionHandler?", e);
		}
	}

	@Override
	public Collection<Class<? extends Perception>> subscribed(Sensor<?> arg) {
		return null; // this.sensorMap.get(arg.getClass()).toCollection();
	}

	/**
	 * Subscribes a {@link Sensor} so that is may receive perceptions of a given type. The {@link Perception}s that are
	 * subscribed to are statically declared by the {@link Sensor} class using the {@link SensiblePerception}
	 * annotation.
	 * 
	 * @param sensor
	 *            : to subscribe
	 */
	@Override
	public void subscribe(Sensor<?> sensor) {
		sensorMap.get(sensor.getClass()).forEach(p -> perceptionMap.get(p).get(sensor.getBody().getId()).add(sensor));
	}

	/**
	 * Unsubscribes a {@link Sensor} so that no longer receives {@link Perception}s from the {@link Environment}. see
	 * also {@link SensorSubscriptionHandler#subscribe(Sensor)}.
	 * 
	 * @param sensor
	 *            : to unsubscribe
	 */
	@Override
	public void unsubscribe(Sensor<?> sensor) {
		sensorMap.get(sensor.getClass())
				.forEach(p -> perceptionMap.get(p).get(sensor.getBody().getId()).remove(sensor));
	}

	@Override
	public void subscribe(Sensor<?> sensor, Class<? extends Perception> perception) {
		// TODO? do we want non static subscription?
	}

	@Override
	public void unsubscribe(Sensor<?> arg1, Class<? extends Perception> arg2) {
		// TODO? do we want non static subscription?
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName()).append(":").append(
				System.lineSeparator());
		builder.append(sensorMap);
		builder.append("Sensors: ").append(System.lineSeparator());
		perceptionMap.forEach((a, b) -> builder.append(a.getSimpleName()).append("->").append(b).append(",")
				.append(System.lineSeparator()));
		return builder.toString();
	}

	public class SensiblePerceptionMap extends
			DefaultAnnotationInheritanceMap<Class<? extends Perception>, SensiblePerception> {

		public SensiblePerceptionMap(Collection<Class<?>> sensors) {
			super(sensors, SensiblePerception.class, SensiblePerception::value, AbstractPassiveSensor.class,
					AbstractActiveSensor.class);
		}
	}
}
