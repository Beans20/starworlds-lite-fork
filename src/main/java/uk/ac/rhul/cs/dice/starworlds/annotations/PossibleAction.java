package uk.ac.rhul.cs.dice.starworlds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import uk.ac.rhul.cs.dice.starworlds.actions.Action;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Actuator;

/**
 * The class level annotation that should be used to signify that an {@link Actuator} is able to attempt a given set of
 * {@link Action}s, given by {@link PossibleAction#value()}.
 * 
 * @author Ben Wilkins
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PossibleAction {
	Class<? extends EnvironmentalAction>[] value() default {};
}
