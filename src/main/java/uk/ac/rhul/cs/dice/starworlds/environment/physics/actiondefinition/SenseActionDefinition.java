package uk.ac.rhul.cs.dice.starworlds.environment.physics.actiondefinition;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset.SenseActionRuleSet;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;
import uk.ac.rhul.cs.dice.starworlds.perception.factory.PerceptionFactory;

public class SenseActionDefinition<E extends Environment<E>, P extends Perception> extends
		ActionDefinition<SenseAction, E> {

	public SenseActionDefinition(PerceptionFactory<P> factory) {
		super(SenseAction.class, new SenseActionRuleSet<SenseAction, E, P>(factory));
	}

}
