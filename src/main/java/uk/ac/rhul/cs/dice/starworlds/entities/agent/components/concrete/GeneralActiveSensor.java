package uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete;

import uk.ac.rhul.cs.dice.starworlds.annotations.SensiblePerception;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.AbstractActiveSensor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;

/**
 * A general concrete {@link AbstractActiveSensor}. It declares no new {@link SensiblePerception}s, inheriting only those
 * from its parent classes.
 * 
 * @author Ben Wilkins
 * @param <K>
 *            {@link ComponentKey} type.
 */
public class GeneralActiveSensor<K extends ComponentKey> extends AbstractActiveSensor<K> {

	public GeneralActiveSensor(K componentKey) {
		super(componentKey);
	}

}
