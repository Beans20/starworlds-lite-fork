package uk.ac.rhul.cs.dice.starworlds.environment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.PhysicalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.EnvironmentAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.AbstractManifest;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.PassiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAutonomousAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Sensor;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.AbstractAmbient;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.Ambient;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.AbstractPhysics;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.Physics;
import uk.ac.rhul.cs.dice.starworlds.environment.subscription.SensorSubscriptionHandler;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

/**
 * The simplest abstract implementation of an {@link Environment}. This abstract class should be extended by any
 * {@link Environment} that will be used for simulation (see {@link Universe}).
 * 
 * @param <E>
 *            the type of environment, should generally be class of environment highest in the class heirarchy (i.e. the
 *            class of the environment that was instantiated).
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class AbstractEnvironment<E extends Environment<E>> extends AbstractManifest implements Environment<E> {

	private List<SenseAction> senseActions;
	private List<PhysicalAction> physicalActions;
	private List<CommunicationAction> communicationActions;

	protected AbstractAmbient ambient;
	protected AbstractPhysics<E> physics;

	protected SensorSubscriptionHandler subscriber;

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            :
	 * 
	 * @param cls
	 *            : the class of this {@link Environment}
	 * @param ambient
	 *            : a {@link Ambient} instance
	 * @param physics
	 *            : the {@link Physics} of the environment
	 * @param subscriptionHandler
	 *            : the {@link AbstractSubscriptionHandler} that will handle subscriptions in this {@link Environment}.
	 * @param appearance
	 *            : the {@link Appearance} of the environment
	 * @param bounded
	 *            : a {@link Boolean} value indicating whether the environment is bounded or not
	 */
	public AbstractEnvironment(String id, Class<E> cls, AbstractAmbient ambient, AbstractPhysics<E> physics,
			SensorSubscriptionHandler subscriptionHandler, EnvironmentAppearance appearance) {
		super(id, appearance);
		init(cls, ambient, physics, subscriptionHandler, appearance);
	}

	private void init(Class<E> cls, AbstractAmbient ambient, AbstractPhysics<E> physics,
			SensorSubscriptionHandler subscriber, EnvironmentAppearance appearance) {
		this.ambient = ambient;
		this.physics = physics;
		this.physics.setEnvironment(cls.cast(this));
		this.subscriber = subscriber;
		this.ambient.getActiveBodies().forEach(this::subscribeToEnvironment);
		this.ambient.getAgents().forEach(this::subscribeToEnvironment);
		this.ambient.getAvatars().forEach(this::subscribeToEnvironment);
		// TODO possible the others depends on changes to the ambient!
		this.communicationActions = new ArrayList<>();
		this.physicalActions = new ArrayList<>();
		this.senseActions = new ArrayList<>();
		this.getAppearance().manifest(this);
	}

	/**
	 * Get all the {@link Sensor}s who subscribe to the given class of {@link Perception}s. <br>
	 * See: {@link SensorSubscriptionHandler#getSensors(Class)}
	 * 
	 * @param pcls
	 *            : class of {@link Perception}
	 * @return A {@link Map} containing the {@link ActiveBody}s id as a key and a list of its {@link Sensor} who have
	 *         subscribed to the given {@link Perception} class.
	 */
	public <P extends Perception> Map<String, List<Sensor<?>>> getSubscribingSensors(Class<P> pcls) {
		return this.subscriber.getSensors(pcls);
	}

	@Override
	public void notifySensors(Perception perception, Collection<String> agents) {
		Map<String, List<Sensor<?>>> sensors = this.subscriber.getSensors(perception.getClass());
		List<Sensor<?>> defaultt = new ArrayList<>(0);
		agents.stream().forEach(a -> sensors.getOrDefault(a, defaultt).forEach(s -> s.notify(perception)));
	}

	@Override
	public void notifySensors(Perception perception) {
		Map<String, List<Sensor<?>>> sensors = this.subscriber.getSensors(perception.getClass());
		sensors.values().stream().forEach(l -> l.forEach(s -> s.notify(perception)));
	}

	@Override
	public void notifySensors(Perception perception, String agent) {
		Map<String, List<Sensor<?>>> sensors = this.subscriber.getSensors(perception.getClass());
		sensors.get(agent).forEach(s -> s.notify(perception));
	}

	@Override
	public void notifySensors(Perception perception, SenseAction action) {
		((Sensor<?>) this.getState().getAgent(action.getActor().getId()).getComponent(action.getComponentKey()))
				.notify(perception);
	}

	public void subscribeToEnvironment(ActiveBody<?> body) {
		body.setEnvironment(this);
		subscriber.subscribeActiveBody(body);
	}

	@Override
	public SensorSubscriptionHandler getSubscriptionHandler() {
		return this.subscriber;
	}

	/**
	 * Subscribes the given {@link AbstractAvatarAgent}s {@link Sensor}s to this {@link Environment} and adds it to this
	 * {@link Environment}s {@link Ambient}. This method should only be called outside of out the {@link Environment}
	 * cycle to prevent conflict of access, consider calling in {@link AbstractPhysics#cycleAddition()} or using
	 * {@link AbstractEnvironment#safeAddAvatar(AbstractAvatarAgent)} instead.
	 * 
	 * @param avatar
	 *            : to add
	 */
	public void addAvatar(AbstractAvatarAgent<?> avatar) {
		// TODO
		this.ambient.addAvatar(avatar);
	}

	/**
	 * Subscribes the given {@link AbstractAutonomousAgent}s {@link Sensor}s to this {@link Environment} and adds it to
	 * this {@link Environment}s {@link Ambient}. This method should only be called outside of out the
	 * {@link Environment} cycle to prevent conflict of access, consider calling in
	 * {@link AbstractPhysics#cycleAddition()} or using
	 * {@link AbstractEnvironment#safeAddAgent(AbstractAutonomousAgent)} instead.
	 * 
	 * @param agent
	 *            : to add
	 */
	public void addAgent(AbstractAutonomousAgent<?> agent) {
		this.ambient.addAgent(agent);
		agent.setEnvironment(this);
	}

	/**
	 * Subscribes the given {@link ActiveBody}s {@link Sensor}s to this {@link Environment} and adds it to this
	 * {@link Environment}s {@link Ambient}. This method should only be called outside of out the {@link Environment}
	 * cycle to prevent conflict of access, consider calling in {@link AbstractPhysics#cycleAddition()} or using
	 * {@link AbstractEnvironment#safeAddActiveBody(ActiveBody)} instead.
	 * 
	 * @param body
	 *            : to add
	 */
	public void addActiveBody(ActiveBody<?> body) {
		this.ambient.addActiveBody(body);
		body.setEnvironment(this);
	}

	/**
	 * Adds the {@link PassiveBody} to this {@link Environment}s {@link Ambient} . This method should only be called
	 * outside of out the {@link Environment} cycle to prevent conflict of access, consider calling in
	 * {@link AbstractPhysics#cycleAddition()} or using {@link AbstractEnvironment#safeAddPassiveBody(PassiveBody)}
	 * instead.
	 * 
	 * @param agent
	 *            : to add
	 */
	public void addPassiveBody(PassiveBody body) {
		this.ambient.addPassiveBody(body);
	}

	@Override
	public void attemptAction(EnvironmentalAction action) {
		if (action instanceof PhysicalAction) {
			attemptPhysicalAction((PhysicalAction) action);
		} else if (action instanceof CommunicationAction) {
			attemptCommunicationAction((CommunicationAction) action);
		} else if (action instanceof SenseAction) {
			attemptSenseAction((SenseAction) action);
		}
	}

	@Override
	public void attemptSenseAction(SenseAction action) {
		this.senseActions.add(action);
	}

	@Override
	public void attemptPhysicalAction(PhysicalAction action) {
		this.physicalActions.add(action);
	}

	@Override
	public void attemptCommunicationAction(CommunicationAction action) {
		this.communicationActions.add(action);
	}

	@Override
	public Collection<CommunicationAction> flushCommunicationActions() {
		List<CommunicationAction> actions = new ArrayList<>();
		actions.addAll(this.communicationActions);
		this.communicationActions.clear();
		return actions;
	}

	@Override
	public Collection<PhysicalAction> flushPhysicalActions() {
		List<PhysicalAction> actions = new ArrayList<>();
		actions.addAll(this.physicalActions);
		this.physicalActions.clear();
		return actions;
	}

	@Override
	public Collection<SenseAction> flushSensingActions() {
		List<SenseAction> actions = new ArrayList<>();
		actions.addAll(this.senseActions);
		this.senseActions.clear();
		return actions;
	}

	@Override
	public List<SenseAction> getSensingActions() {
		return senseActions;
	}

	@Override
	public List<PhysicalAction> getPhysicalActions() {
		return physicalActions;
	}

	@Override
	public List<CommunicationAction> getCommunicationActions() {
		return communicationActions;
	}

	@Override
	public AbstractAmbient getState() {
		return this.ambient;
	}

	@Override
	public void setState(Ambient state) {
		this.ambient = (AbstractAmbient) state;
	}

	@Override
	public AbstractPhysics<E> getPhysics() {
		return this.physics;
	}

	@Override
	public void setPhysics(Physics<E> physics) {
		this.physics = (AbstractPhysics<E>) physics;
	}

	@Override
	public EnvironmentAppearance getAppearance() {
		return (EnvironmentAppearance) super.getAppearance();
	}

	public synchronized SensorSubscriptionHandler getSubscriber() {
		return subscriber;
	}

	@Override
	public String toString() {
		return this.getAppearance().toString();
	}
}