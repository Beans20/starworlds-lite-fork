package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key;

import uk.ac.rhul.cs.dice.starworlds.entities.PassiveBody;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;

public class PassiveBodyAttributeKey extends AttributeKey<MapAttribute<String, PassiveBody>> {

	public PassiveBodyAttributeKey(Class<MapAttribute<String, PassiveBody>> cls) {
		super(cls);
	}

}
