package uk.ac.rhul.cs.dice.starworlds.entities.avatar;

import java.util.HashMap;
import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;

public abstract class InterfacedAvatarMind<I, G, K extends ComponentKey> extends AbstractAvatarMind<G, K> {

	public abstract I getInterface();

	public abstract void showInterface(I interfac);

	public abstract UserResponse<G> getUserResponse();

	@Override
	public void cycle() {
		UserResponse<G> response;
		Map<K, EnvironmentalAction> actions = new HashMap<>();
		while ((response = getUserResponse()) != null) {
			EnvironmentalAction action = actionmap.get(response.getKey()).create(response.getResponse());
			actions.put(componentmap.get(action.getClass()), action);
		}
		this.getBody().attempt(actions);
	}
}
