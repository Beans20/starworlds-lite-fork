package uk.ac.rhul.cs.dice.starworlds.appearances;

import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializableActiveBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;

public class ActiveBodyAppearance extends PhysicalBodyAppearance {

	public ActiveBodyAppearance() {
		super();
	}

	public ActiveBodyAppearance(ActiveBody<?> body) {
		super(body);
	}

	@Override
	public String represent() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.represent()).append(" Components: ");
		builder = componentsToString(getActiveBody().getComponentsMap(), builder);
		return builder.toString();
	}

	private ActiveBody<?> getActiveBody() {
		return (ActiveBody<?>) this.getManifest();
	}

	protected StringBuilder componentsToString(Map<?, ?> map, StringBuilder builder) {
		map.entrySet().stream()
				.forEach(e -> builder.append(e.getKey()).append("->").append(e.getValue().getClass().getSimpleName()));
		return builder;
	}

	@Override
	public SerializableActiveBodyAppearance getSerializable() {
		return new SerializableActiveBodyAppearance((ActiveBody<?>) this.getManifest());
	}

}