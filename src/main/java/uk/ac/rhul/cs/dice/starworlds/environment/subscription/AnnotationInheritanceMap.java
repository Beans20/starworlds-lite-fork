package uk.ac.rhul.cs.dice.starworlds.environment.subscription;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.function.Function;

public abstract class AnnotationInheritanceMap<T, A extends Annotation> extends InheritanceMap<T> {

	private Class<A> annotationClass;
	private Function<A, Collection<T>> valueRetriever;

	public AnnotationInheritanceMap(Class<A> annotationClass, Function<A, Collection<T>> valueRetriever,
			Class<?>... base) {
		super(base);
		this.annotationClass = annotationClass;
		this.valueRetriever = valueRetriever;
	}

	public AnnotationInheritanceMap(Class<A> annotationClass, Function<A, Collection<T>> valueRetriever,
			Set<Class<?>> base) {
		super(base);
		this.annotationClass = annotationClass;
		this.valueRetriever = valueRetriever;
	}

	public AnnotationInheritanceMap(Class<A> annotationClass, Function<A, Collection<T>> valueRetriever, Class<?> base) {
		super(base);
		this.annotationClass = annotationClass;
		this.valueRetriever = valueRetriever;
	}

	public InheritanceCollection<T> put(Class<?> key) {
		if (!this.containsKey(key)) {
			return super.put(key, newCollection(key));
		} else {
			return this.get(key);
		}
	}

	@Override
	protected Collection<T> newCollection(Class<?> key) {
		if (!containsKey(key)) {
			return valueRetriever.apply(key.getAnnotation(annotationClass));
		} else {
			return new ArrayList<>(0);
		}
	}

	@Override
	public String toString() {
		return super.toString();
	}

}
