package uk.ac.rhul.cs.dice.starworlds.environment.ambient.processes;

import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;

public interface Process extends Identifiable {

}
