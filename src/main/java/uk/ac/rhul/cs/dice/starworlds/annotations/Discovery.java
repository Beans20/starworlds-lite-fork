package uk.ac.rhul.cs.dice.starworlds.annotations;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

// TODO: Auto-generated Javadoc
/**
 * A class path scanner.
 * 
 * @author Ben Wilkins
 *
 */
public class Discovery {

	/** The result of the most recent scan. */
	private static ScanResult result = null;

	/**
	 * Gets the result of the most recent scan.
	 *
	 * @return the scan result
	 */
	public static ScanResult scan() {
		return result;
	}

	/**
	 * Performs a new scan of the class path.
	 *
	 * @param spec
	 *            the spec, see the documnetation of {@link FastClasspathScanner}.
	 * @return the scan result
	 */
	public static ScanResult newScan(String[] spec) {
		FastClasspathScanner scanner = new FastClasspathScanner(spec);
		result = scanner.scan();
		return result;
	}

	/**
	 * Has a class path scan be performed (i.e. when {@link Discovery#scan()} will not return null).
	 *
	 * @return true, if successful
	 */
	public static boolean scanned() {
		return result != null;
	}

}
