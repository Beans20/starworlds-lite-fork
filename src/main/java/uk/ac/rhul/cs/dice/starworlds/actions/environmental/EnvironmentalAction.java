package uk.ac.rhul.cs.dice.starworlds.actions.environmental;

import java.io.Serializable;

import org.omg.CORBA.Environment;

import uk.ac.rhul.cs.dice.starworlds.actions.Action;
import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.Physics;

/**
 * The interface for all the {@link Action}s that should be performed in some {@link Environment} by some
 * {@link Physics}.<br>
 * 
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface EnvironmentalAction extends Action, Identifiable, Serializable {

}