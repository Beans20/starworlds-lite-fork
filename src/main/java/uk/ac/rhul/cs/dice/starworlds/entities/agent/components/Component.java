package uk.ac.rhul.cs.dice.starworlds.entities.agent.components;

import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;

/**
 * A part of an {@link ActiveBody}. See {@link Sensor} and {@link Actuator}.
 * 
 * @author Ben Wilkins
 *
 */
public interface Component<K extends ComponentKey> extends Identifiable {

	public K getComponentKey();

	public ActiveBody<K> getBody();

	public void setBody(ActiveBody<K> body);
}
