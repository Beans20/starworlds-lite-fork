package uk.ac.rhul.cs.dice.starworlds.environment.subscription;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InheritanceCollection<V> implements Collection<V> {

	protected Collection<V> values;
	protected InheritanceCollection<V> inherited = null;

	public InheritanceCollection(Collection<V> values) {
		this.values = values;
	}

	@Override
	public void forEach(Consumer<? super V> action) {
		values.forEach(action);
		if (inherited != null) {
			inherited.forEach(action);
		}
	}

	@Override
	public boolean add(V e) {
		return values.add(e);
	}

	@Override
	public boolean addAll(Collection<? extends V> c) {
		return values.addAll(c);
	}

	@Override
	public void clear() {
		values.clear();
	}

	@Override
	public boolean contains(Object o) {
		return values.contains(o) || ((inherited != null) ? inherited.contains(o) : false);
	}

	public boolean nonInheritedContains(Object o) {
		return values.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return this.stream().collect(Collectors.toSet()).containsAll(new HashSet<>(c));
	}

	@Override
	public boolean equals(Object o) {
		return values.equals(o);
	}

	@Override
	public int hashCode() {
		return values.hashCode();
	}

	@Override
	public boolean isEmpty() {
		return values.isEmpty();
	}

	/**
	 * TODO
	 */
	@Override
	public Iterator<V> iterator() {
		return null;
	}

	@Override
	public Stream<V> parallelStream() {
		return Stream.of(this.values.parallelStream(), inherited.parallelStream()).flatMap(s -> s);
	}

	@Override
	public Stream<V> stream() {
		return Stream.of(this.values.stream(), inherited.stream()).flatMap(s -> s);
	}

	@Override
	public boolean remove(Object o) {
		return values.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return values.removeAll(c);
	}

	@Override
	public boolean removeIf(Predicate<? super V> filter) {
		return values.removeIf(filter);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return values.retainAll(c);
	}

	@Override
	public int size() {
		return values.size() + ((inherited != null) ? inherited.size() : 0);
	}

	/**
	 * TODO
	 */
	@Override
	public Spliterator<V> spliterator() {
		return null;
	}

	@Override
	public Object[] toArray() {
		Collection<Object[]> arrays = new ArrayList<>();
		toArrays(arrays);

		return combineArrays(arrays);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a) {
		Collection<Object[]> arrays = new ArrayList<>();
		toArrays(arrays);
		return (T[]) combineArrays(arrays);
	}

	private void toArrays(Collection<Object[]> arrays) {
		arrays.add(values.toArray());
		if (inherited != null) {
			inherited.toArrays(arrays);
		}
	}

	private Object[] combineArrays(Collection<Object[]> arrays) {
		int size = 0;
		for (Object[] t : arrays) {
			size += t.length;
		}
		Object[] array = new Object[size];
		int pos = 0;
		for (Object[] t : arrays) {
			System.arraycopy(t, 0, array, pos, t.length);
			pos += t.length;
		}
		return array;
	}

	public InheritanceCollection<V> getInherited() {
		return inherited;
	}

	public void setInherited(InheritanceCollection<V> inherited) {
		this.inherited = inherited;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("{");
		this.valuesToString(builder);
		if (builder.length() > 1) {
			builder.deleteCharAt(builder.length() - 1);
		}
		builder.append("}");
		return builder.toString();
	}

	protected StringBuilder valuesToString(StringBuilder builder) {
		if (values != null) {
			this.values.forEach(v -> builder.append(v).append(","));
		}
		if (inherited != null) {
			return inherited.valuesToString(builder);
		}
		return builder;

	}
}