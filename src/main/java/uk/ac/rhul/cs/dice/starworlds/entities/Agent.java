package uk.ac.rhul.cs.dice.starworlds.entities;

import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAutonomousAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractMind;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;

/**
 * The interface for agents.<br/>
 * <br/>
 * 
 * Known implementations: {@link AbstractAutonomousAgent}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface Agent<K extends ComponentKey> extends Actor {
	public abstract AbstractMind<K> getMind();
}