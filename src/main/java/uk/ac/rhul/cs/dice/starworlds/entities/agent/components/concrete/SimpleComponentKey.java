package uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete;

import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;

public enum SimpleComponentKey implements ComponentKey {

	ACTUATOR, PASSIVESENSOR, ACTIVESENSOR;

}
