package uk.ac.rhul.cs.dice.starworlds.entities.avatar;

import java.util.Collection;

import uk.ac.rhul.cs.dice.starworlds.appearances.ActiveBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgentMind;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractMind;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Actuator;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Component;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Sensor;

public abstract class AbstractAvatarAgent<K extends ComponentKey> extends AbstractAgent<K> implements Avatar {

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            :
	 * @param components
	 *            : collection of {@link Sensor}s and {@link Actuator}s to be used by this {@link ActiveBody}
	 * @param mind
	 *            : the {@link AbstractAgentMind}.
	 * 
	 */
	public <G> AbstractAvatarAgent(String id, Collection<Component<K>> components, AbstractAvatarMind<G, K> mind) {
		super(id, components, mind);
	}

	/**
	 * Constructor.
	 * 
	 * @param appearance
	 *            : the {@link ActiveBodyAppearance}.
	 * @param components
	 *            : collection of {@link Sensor}s and {@link Actuator}s to be used by this {@link ActiveBody}
	 * @param mind
	 *            : the {@link AbstractMind}.
	 */
	public <G> AbstractAvatarAgent(String id, ActiveBodyAppearance appearance, Collection<Component<K>> components,
			AbstractAvatarMind<G, K> mind) {
		super(id, appearance, components, mind);
	}

	@Override
	public void run() {
		this.mind.cycle();
	}
}
