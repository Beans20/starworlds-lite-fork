package uk.ac.rhul.cs.dice.starworlds.appearances;

import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializableAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.Manifest;

/**
 * The interface for appearances. An {@link Appearance} is always the appearance of a {@link Manifest}. A manifest is
 * something which has the property of appearance.
 * 
 * Known implementations: {@link AbstractAppearance}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface Appearance extends Identifiable {
	/**
	 * Returns a {@link String} representation of the {@link Appearance}
	 * 
	 * @return a {@link String} representation of the {@link Appearance}
	 */
	public String represent();

	/**
	 * Initialises this {@link Appearance} with the {@link Manifest} that it is the {@link Appearance} of.
	 * 
	 * @param manifest
	 *            : that this is the appearance of
	 */
	public void manifest(Manifest manifest);

	/**
	 * Gets or constructs a {@link SerializableAppearance} that reflects this appearance.
	 * 
	 * @return the {@link SerializableAppearance}
	 */
	public SerializableAppearance getSerializable();

}