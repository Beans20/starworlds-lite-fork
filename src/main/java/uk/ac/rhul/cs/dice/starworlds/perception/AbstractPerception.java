package uk.ac.rhul.cs.dice.starworlds.perception;

import uk.ac.rhul.cs.dice.starworlds.event.AbstractEvent;

/**
 * An abstract implementation of {@link Perception}. All {@link Perception}s should extend this class. </br> Implements:
 * {@link Perception} </br> Known direct subclasses: {@link DefaultPerception}, {@link CommunicationPerception}
 * 
 * @author Ben
 *
 */
public abstract class AbstractPerception extends AbstractEvent implements Perception {

	private static final long serialVersionUID = 7996440175185200462L;

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

}
