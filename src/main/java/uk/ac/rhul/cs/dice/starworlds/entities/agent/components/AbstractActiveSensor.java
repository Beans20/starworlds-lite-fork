package uk.ac.rhul.cs.dice.starworlds.entities.agent.components;

import java.util.HashSet;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.annotations.PossibleAction;
import uk.ac.rhul.cs.dice.starworlds.annotations.SensiblePerception;
import uk.ac.rhul.cs.dice.starworlds.perception.ActivePerception;
import uk.ac.rhul.cs.dice.starworlds.perception.NullPerception;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

/**
 * An abstract class implementing {@link Sensor}. //TODO <br/>
 * 
 * Known direct subclasses: none.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
@PossibleAction({ SenseAction.class })
@SensiblePerception({ NullPerception.class, ActivePerception.class })
public abstract class AbstractActiveSensor<K extends ComponentKey> extends AbstractActuator<K> implements Sensor<K> {

	private Set<Perception> perceptions;

	// private boolean open = false;

	/**
	 * Constructor.
	 */
	public AbstractActiveSensor(K componentKey) {
		super(componentKey);
		this.perceptions = new HashSet<>();
	}

	@Override
	public void attempt(EnvironmentalAction action) {
		((SenseAction) action).setComponentKey(this.getComponentKey());
		super.attempt(action);
	}

	@Override
	public void notify(Perception perception) {
		this.perceptions.add(perception);
		this.getBody().sensorActive(this);
	}

	/**
	 * Getter for the {@link Perception}s currently stored in the internal buffer of this {@link AbstractActiveSensor}.
	 * These are all of the perceptions that were received by this {@link AbstractActiveSensor} in the previous cycle.
	 * The internal buffer of {@link Perception}s will be cleared after this method call.
	 * 
	 * @return the set of {@link Perception}s
	 */
	@Override
	public Set<Perception> getPerceptions() {
		HashSet<Perception> result = new HashSet<>(this.perceptions);
		this.perceptions.clear();
		return result;
	}

	@Override
	public String toString() {
		return this.getId() + ":" + this.getClass().getSimpleName();
	}

	// @Override
	// public boolean isOpen() {
	// return open;
	// }
	//
	// @Override
	// public void setClosed() {
	// this.open = false;
	// }
	//
	// @Override
	// public void setOpen() {
	// this.open = true;
	// }
}