package uk.ac.rhul.cs.dice.starworlds.event;

import uk.ac.rhul.cs.dice.starworlds.entities.PhysicalBody;

public class TransferEvent extends AbstractEvent {

	private static final long serialVersionUID = -8455015894024842488L;

	private PhysicalBody body;

	public TransferEvent(PhysicalBody body) {
		this.setBody(body);
	}

	public PhysicalBody getBody() {
		return body;
	}

	public void setBody(PhysicalBody body) {
		this.body = body;
	}
}
