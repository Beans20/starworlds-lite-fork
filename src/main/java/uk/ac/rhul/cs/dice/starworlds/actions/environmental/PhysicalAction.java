package uk.ac.rhul.cs.dice.starworlds.actions.environmental;

/**
 * An {@link EnvironmentalAction} representing a physical action.
 * 
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class PhysicalAction extends AbstractEnvironmentalAction {

	private static final long serialVersionUID = -6022492073801887027L;

}