package uk.ac.rhul.cs.dice.starworlds.entities.avatar.concrete;

import java.util.Collection;

import uk.ac.rhul.cs.dice.starworlds.appearances.ActiveBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Component;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimpleComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.concrete.SimpleAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarMind;
import uk.ac.rhul.cs.dice.starworlds.initialisation.IDFactory;

public class SimpleAvatar extends AbstractAvatarAgent<SimpleComponentKey> {

	public <G> SimpleAvatar(ActiveBodyAppearance appearance, Collection<Component<SimpleComponentKey>> components,
			AbstractAvatarMind<G, SimpleComponentKey> mind) {
		super(IDFactory.getInstance().getNewID(), appearance, components, mind);
	}

	public <G> SimpleAvatar(Collection<Component<SimpleComponentKey>> components,
			AbstractAvatarMind<G, SimpleComponentKey> mind) {
		super(IDFactory.getInstance().getNewID(), components, mind);
	}

	public <G> SimpleAvatar(AbstractAvatarMind<G, SimpleComponentKey> mind) {
		super(IDFactory.getInstance().getNewID(), SimpleAgent.getGeneralComponents(), mind);
	}
}
