package uk.ac.rhul.cs.dice.starworlds.entities.agent.components;

import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

/**
 * 
 * TODO Known implementations: {@link AbstractActiveSensor}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 * 
 * @param <A>
 *            TODO
 */
public interface Sensor<K extends ComponentKey> extends Component<K> {

	/**
	 * Get all of the {@link Perception} sensed by this {@link Sensor}.
	 * 
	 * @return the {@link Perception}s
	 */
	public Set<Perception> getPerceptions();

	/**
	 * Notify this {@link Sensor} with a new {@link Perception}.
	 * 
	 * @param perception
	 */
	public void notify(Perception perception);

	// for the next version!
	// public void setOpen();
	//
	// public void setClosed();
	//
	// public boolean isOpen();

}