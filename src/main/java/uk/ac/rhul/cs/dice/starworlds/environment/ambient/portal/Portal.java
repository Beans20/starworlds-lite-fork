package uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal;

import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.entities.Manifest;

public interface Portal extends Manifest {

	public Appearance getRemoteAppearance();

}
