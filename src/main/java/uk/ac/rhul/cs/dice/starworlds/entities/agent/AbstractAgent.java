package uk.ac.rhul.cs.dice.starworlds.entities.agent;

import java.util.Collection;

import uk.ac.rhul.cs.dice.starworlds.appearances.ActiveBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.Agent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Actuator;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Component;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Sensor;

public abstract class AbstractAgent<K extends ComponentKey> extends ActiveBody<K> implements Agent<K> {

	protected AbstractMind<K> mind;

	/**
	 * Constructor.
	 * 
	 * @param components
	 *            : collection of {@link Sensor}s and {@link Actuator}s to be used by this {@link ActiveBody}
	 * @param mind
	 *            : the {@link AbstractAgentMind}.
	 * 
	 */
	public AbstractAgent(String id, Collection<Component<K>> components, AbstractMind<K> mind) {
		super(id, components);
		init(mind);
	}

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            :
	 * 
	 * @param appearance
	 *            : the {@link ActiveBodyAppearance}.
	 * @param components
	 *            : collection of {@link Sensor}s and {@link Actuator}s to be used by this {@link ActiveBody}
	 * @param mind
	 *            : the {@link AbstractMind}.
	 */
	public AbstractAgent(String id, ActiveBodyAppearance appearance, Collection<Component<K>> components,
			AbstractMind<K> mind) {
		super(id, appearance, components);
		init(mind);
	}

	private void init(AbstractMind<K> mind) {
		this.mind = mind;
		this.mind.setBody(this);
	}

	/**
	 * Returns the {@link AbstractMind} of the {@link AbstractAgent} .
	 * 
	 * @return the {@link AbstractMind} of the {@link AbstractAgent} .
	 */
	@Override
	public AbstractMind<K> getMind() {
		return this.mind;
	}

	@Override
	public void run() {
		this.mind.cycle();
	}
}
