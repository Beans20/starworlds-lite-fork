package uk.ac.rhul.cs.dice.starworlds.environment.physics.actiondefinition;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset.ActionRuleSet;

public class ActionDefinition<A extends EnvironmentalAction, E extends Environment<E>> {

	private Class<A> actionclass;
	private ActionRuleSet<A, E> ruleset;

	public ActionDefinition(Class<A> actionclass, ActionRuleSet<A, E> ruleset) {
		super();
		this.actionclass = actionclass;
		this.ruleset = ruleset;
	}

	public Class<A> getActionclass() {
		return actionclass;
	}

	public void setActionclass(Class<A> actionclass) {
		this.actionclass = actionclass;
	}

	public ActionRuleSet<A, E> getRuleset() {
		return ruleset;
	}

	public void setRuleset(ActionRuleSet<A, E> ruleset) {
		this.ruleset = ruleset;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actionclass == null) ? 0 : actionclass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActionDefinition<?, ?> other = (ActionDefinition<?, ?>) obj;
		if (actionclass == null) {
			if (other.actionclass != null)
				return false;
		} else if (!actionclass.equals(other.actionclass))
			return false;
		return true;
	}
}
