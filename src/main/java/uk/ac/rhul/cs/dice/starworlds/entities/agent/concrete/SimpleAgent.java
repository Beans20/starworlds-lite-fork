package uk.ac.rhul.cs.dice.starworlds.entities.agent.concrete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import uk.ac.rhul.cs.dice.starworlds.appearances.ActiveBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgentMind;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAutonomousAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Component;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.GeneralActiveSensor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.GeneralActuator;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.GeneralPassiveSensor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimpleComponentKey;
import uk.ac.rhul.cs.dice.starworlds.initialisation.IDFactory;

public class SimpleAgent extends AbstractAutonomousAgent<SimpleComponentKey> {

	public SimpleAgent(ActiveBodyAppearance appearance, Collection<Component<SimpleComponentKey>> components,
			AbstractAgentMind<SimpleComponentKey> mind) {
		super(IDFactory.getInstance().getNewID(), appearance, components, mind);
	}

	public SimpleAgent(Collection<Component<SimpleComponentKey>> components, AbstractAgentMind<SimpleComponentKey> mind) {
		super(IDFactory.getInstance().getNewID(), components, mind);
	}

	public SimpleAgent(AbstractAgentMind<SimpleComponentKey> mind) {
		super(IDFactory.getInstance().getNewID(), getGeneralComponents(), mind);
	}

	public static Collection<Component<SimpleComponentKey>> getGeneralComponents() {
		List<Component<SimpleComponentKey>> components = new ArrayList<>();
		components.add(new GeneralActiveSensor<>(SimpleComponentKey.ACTIVESENSOR));
		components.add(new GeneralPassiveSensor<SimpleComponentKey>(SimpleComponentKey.PASSIVESENSOR));
		components.add(new GeneralActuator<SimpleComponentKey>(SimpleComponentKey.ACTUATOR));
		return components;
	}
}
