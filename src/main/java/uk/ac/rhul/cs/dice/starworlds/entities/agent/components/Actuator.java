package uk.ac.rhul.cs.dice.starworlds.entities.agent.components;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;

/**
 * The interface for actuators. It extends {@link CustomObserver}.<br/>
 * <br/>
 * 
 * Known implementations: {@link AbstractActuator}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface Actuator<K extends ComponentKey> extends Component<K> {

	public void attempt(EnvironmentalAction action);

}