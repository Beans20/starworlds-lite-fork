package uk.ac.rhul.cs.dice.starworlds.appearances.serializable;

import java.io.Serializable;

import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;

/**
 * A serializable version of an {@link Appearance}. See {@link Appearance#getSerializable()}.
 * 
 * @author Ben Wilkins
 *
 */
public interface SerializableAppearance extends Appearance, Serializable {

}
