package uk.ac.rhul.cs.dice.starworlds.entities.agent.components;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;

/**
 * The most generic class implementing {@link Actuator}. <br/>
 * 
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class AbstractActuator<K extends ComponentKey> extends AbstractComponent<K> implements Actuator<K> {

	public AbstractActuator(K componentKey) {
		super(componentKey);

	}

	@Override
	public void attempt(EnvironmentalAction action) {
		this.getBody().getEnvironment().attemptAction(action);
	}

}