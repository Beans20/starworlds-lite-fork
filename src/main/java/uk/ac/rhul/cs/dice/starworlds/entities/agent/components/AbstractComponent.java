package uk.ac.rhul.cs.dice.starworlds.entities.agent.components;

import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;
import uk.ac.rhul.cs.dice.starworlds.initialisation.IDFactory;

public abstract class AbstractComponent<K extends ComponentKey> implements Component<K> {

	private String id;
	private ActiveBody<K> body;
	private K componentKey = null;

	public AbstractComponent(K componentKey) {
		id = IDFactory.getInstance().getNewID();
		this.componentKey = componentKey;
	}

	@Override
	public K getComponentKey() {
		return componentKey;
	}

	@Override
	public ActiveBody<K> getBody() {
		return this.body;
	}

	@Override
	public void setBody(ActiveBody<K> body) {
		if (this.body == null) {
			this.body = body;
		} else {
			throw new IllegalComponentAssignmentException();
		}

	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " : " + this.getId();
	}

	private static class IllegalComponentAssignmentException extends StarWorldsRuntimeException {
		private static final long serialVersionUID = -4700370346308424316L;

		public IllegalComponentAssignmentException() {
			super("Cannot assign a Component to multiple bodies");
		}
	}
}
