package uk.ac.rhul.cs.dice.starworlds.annotations;

import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.actions.Action;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.AbstractActuator;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Actuator;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.subscription.DefaultAnnotationInheritanceMap;
import uk.ac.rhul.cs.dice.starworlds.environment.subscription.InheritanceMap;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

// TODO: Auto-generated Javadoc
/**
 * Discovers all types annotated with {@link PossibleAction} and stores them as a map.
 * 
 * @author Ben Wilkins
 *
 */
public class PossibleActionDiscovery {

	/** The Constant SPEC. */
	private static final List<String> SPEC = new ArrayList<>();

	/**
	 * The Constant POSSIBLEACTIONS, an {@link InheritanceMap} created after the scan for the {@link PossibleAction}
	 * annotation. The keys are subtypes of {@link Actuator}, the value is the set of possible actions that can be
	 * attempted by an instance {@link Actuator} (as declared by the {@link PossibleAction} annotation).
	 */
	private static final PossibleActionMap POSSIBLEACTIONS;

	/** The Constant POSSIBLEACTIONSET. */
	private static final Set<Class<? extends EnvironmentalAction>> POSSIBLEACTIONSET = new HashSet<>();

	/**
	 * Instantiates a new possible action discovery.
	 */
	private PossibleActionDiscovery() {
	}

	static {
		// TODO... add to this?
		SPEC.add("-" + Action.class.getPackage().getName());
		SPEC.add("-" + Perception.class.getPackage().getName());
		SPEC.add("-" + Environment.class.getPackage().getName());
		PossibleActionMap map = null;
		try {
			map = discover();
		} catch (ClassNotFoundException e) {
			throw new StarWorldsRuntimeException("Failed to discover possible actions, see: "
					+ PossibleActionDiscovery.class, e);
		}
		POSSIBLEACTIONS = map;
	}

	/**
	 * Gets all possible action that could be attempted system wide.
	 *
	 * @return the all possible action
	 */
	public static synchronized Set<Class<? extends EnvironmentalAction>> getAllPossibleAction() {
		return Collections.unmodifiableSet(POSSIBLEACTIONSET);
	}

	/**
	 * Gets the map of possible actions..
	 *
	 * @return the possible actions
	 */
	public static synchronized Map<Class<?>, Collection<Class<? extends EnvironmentalAction>>> getPossibleActions() {
		return Collections.unmodifiableMap(POSSIBLEACTIONS);
	}

	/**
	 * Gets the possible actions.
	 *
	 * @param actuator
	 *            the actuator
	 * @return the possible actions
	 */
	public static synchronized Collection<Class<? extends EnvironmentalAction>> getPossibleActions(Class<?> actuator) {
		return Collections.unmodifiableCollection(POSSIBLEACTIONS.get(actuator));
	}

	/**
	 * Discover.
	 *
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 */
	private static PossibleActionMap discover() throws ClassNotFoundException {
		// FastClasspathScanner scanner = new FastClasspathScanner(SPEC.toArray(new String[] {}));
		// ScanResult result = scanner.scan();
		ScanResult result = Discovery.newScan(SPEC.toArray(new String[] {}));
		List<String> classes = result.getNamesOfClassesImplementing(Actuator.class);
		Collection<Class<?>> actuators = new HashSet<>();
		for (String cls : classes) {
			actuators.add(Class.forName(cls));
		}
		return new PossibleActionMap(actuators);
	}

	public static class PossibleActionMap extends
			DefaultAnnotationInheritanceMap<Class<? extends EnvironmentalAction>, PossibleAction> {
		public PossibleActionMap(Collection<Class<?>> keys) {
			super(keys, PossibleAction.class, PossibleAction::value, AbstractActuator.class);
		}
	}
}
