package uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete;

import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.AbstractActiveSensor;

public class SimpleActiveSensor extends AbstractActiveSensor<SimpleComponentKey> {

	public SimpleActiveSensor() {
		super(SimpleComponentKey.ACTIVESENSOR);
	}

}
