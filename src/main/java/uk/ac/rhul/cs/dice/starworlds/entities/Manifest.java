package uk.ac.rhul.cs.dice.starworlds.entities;

import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;

/**
 * Something is manifest when it has an {@link Appearance}.
 * 
 * @author Ben Wilkins
 *
 */
public interface Manifest extends Identifiable {

	public Appearance getAppearance();

}
