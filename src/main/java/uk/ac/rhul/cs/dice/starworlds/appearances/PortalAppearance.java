package uk.ac.rhul.cs.dice.starworlds.appearances;

import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializableAppearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializablePortalAppearance;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AbstractPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.Portal;

public class PortalAppearance extends AbstractAppearance {

	public PortalAppearance() {
		super();
	}

	public Appearance getRemoteAppearance() {
		return this.getManifest().getRemoteAppearance();
	}

	@Override
	protected AbstractPortal getManifest() {
		return (AbstractPortal) super.getManifest();
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public SerializableAppearance getSerializable() {
		return new SerializablePortalAppearance((Portal) this.getManifest());
	}

}
