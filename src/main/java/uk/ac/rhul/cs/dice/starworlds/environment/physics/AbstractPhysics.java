package uk.ac.rhul.cs.dice.starworlds.environment.physics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.actions.Action;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.PhysicalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.Agent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.Simulator;
import uk.ac.rhul.cs.dice.starworlds.environment.concrete.SimplePhysics;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.actiondefinition.ActionDefinition;

/**
 * An abstract implementation of {@link Physics}. This physics handles the Perceive, Decide, Execute cycle of all
 * {@link Agent}s in its {@link Environment} - it is the time keeper. The {@link Physics} is responsible for executing
 * any {@link Action}s that an {@link Agent} or {@link ActiveBody} performs. These {@link Action}s default as follows:
 * </br> {@link SenseAction}, {@link CommunicationAction}, {@link PhysicalAction}. For details on each type see their
 * documentation. </br>
 * 
 * 
 * Known direct subclasses: {@link SimplePhysics}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class AbstractPhysics<E extends Environment<E>> implements Physics<E>, Simulator {

	private static final long DEFAULTFRAMELENGTH = 1000;

	protected boolean info = false;

	protected E environment;

	protected ActionExecutor<E> actionExecutor;
	protected TimeState timestate;

	protected Long time = 0l;
	protected long framelength = DEFAULTFRAMELENGTH;
	protected Simulator simulator = new Simulator();
	protected Thread simulatorThread;

	public enum Time {
		SERIAL, PARALLEL;
	}

	private class Simulator implements Runnable {

		volatile boolean simulating = false;

		@Override
		public void run() {
			while (simulating) {
				if (info) {
					System.out.println("-------- cycle: " + time + " --------");
				}
				AbstractPhysics.this.cycle();
				this.sleep();
			}
		}

		protected void sleep() {
			try {
				Thread.sleep(framelength);
			} catch (InterruptedException e) {
				System.err.println("Simulator interrupted during sleep - something went wrong!");
				Thread.currentThread().interrupt();
			}
		}
	}

	public AbstractPhysics() {
		actionExecutor = new ActionExecutor<>();
		timestate = new TimeStateSerial();
	}

	public AbstractPhysics(Time time) {
		if (time == null) {
			throw new IllegalArgumentException("time argument cannot be null, use empty constructor instead");
		} else if (time.equals(Time.SERIAL)) {
			this.timestate = new TimeStateSerial();
		} else if (time.equals(Time.PARALLEL)) {
			this.timestate = new TimeStateParallel();
		}
		actionExecutor = new ActionExecutor<>();
	}

	public void simulate() {
		simulatorThread = new Thread(simulator);
		simulator.simulating = true;
		simulatorThread.start();
		if (info) {
			System.out.println("Simulating...");
		}
	}

	public void stop() {
		if (info) {
			System.out.println("...Stopping Simulation...");
		}
		simulator.simulating = false;
		try {
			simulatorThread.join();
		} catch (InterruptedException e) {
			System.err.println("Thread interrupt - something went wrong while attempting to end the simlation");
			Thread.currentThread().interrupt();
		}
		if (info) {
			System.out.println("...Stopped");
		}
	}

	protected void cycle() {
		time = updateTime();
		this.runActors();
		this.executeActions();
		this.cycleAddition();
	}

	protected Long updateTime() {
		return this.time + 1;
	}

	@Override
	public Long getTime() {
		return time;
	}

	public abstract void cycleAddition();

	public <A extends EnvironmentalAction> void addActionDefinition(ActionDefinition<A, E> actionDefinition) {
		this.actionExecutor.put(actionDefinition);
	}

	@Override
	public void executeAction(EnvironmentalAction action, E environment) {
		this.actionExecutor.executeAction(action, environment);
	}

	@Override
	public void runActors() {
		timestate.simulate();
	}

	@Override
	public void executeActions() {
		environment.flushPhysicalActions().stream().forEach(x -> this.executeAction(x, environment));
		environment.flushSensingActions().stream().forEach(x -> this.executeAction(x, environment));
		environment.flushCommunicationActions().stream().forEach(x -> this.executeAction(x, environment));
	}

	@Override
	public void setEnvironment(E environment) {
		if (this.environment == null) {
			this.environment = environment;
		}
	}

	@Override
	public E getEnvironment() {
		return this.environment;
	}

	/**
	 * The state of a {@link Physics} that may be either serial or parallel see {@link TimeStateSerial},
	 * {@link TimeStateParallel}. These {@link TimeState}s define the order in which the {@link Agent}s should run and
	 * whether the system is multi-threaded.
	 * 
	 * @author Ben Wilkins
	 *
	 */
	protected interface TimeState {

		public void simulate();
	}

	/**
	 * The implementation of {@link TimeState} for serial, agents will run in an arbitrary order one at a time.
	 * 
	 * @author Ben Wilkins
	 *
	 */
	protected class TimeStateSerial implements TimeState {

		public void simulate() {
			AbstractPhysics.this.environment.getState().getAgents().forEach(AbstractAgent::run);
			AbstractPhysics.this.environment.getState().getActiveBodies().forEach(ActiveBody::run);
			AbstractPhysics.this.environment.getState().getAvatars().forEach(AbstractAvatarAgent::run);
		}
	}

	/**
	 * The implementation of {@link TimeState} for parallel, agents will run in their own thread in parallel.
	 * 
	 * @author Ben Wilkins
	 *
	 */
	protected class TimeStateParallel implements TimeState {

		@Override
		public void simulate() {
			// split into threads
			Collection<Thread> threads = new ArrayList<>();
			getThreads(AbstractPhysics.this.environment.getState().getAgents());
			getThreads(AbstractPhysics.this.environment.getState().getActiveBodies());
			getThreads(AbstractPhysics.this.environment.getState().getAvatars());
			try {
				waitForActors(threads);
			} catch (InterruptedException e) {
				System.err.println("A thread was interrupted during simulation - something went wrong!");
				Thread.currentThread().interrupt();
			}
		}

		private Collection<Thread> getThreads(Collection<? extends Runnable> runnables) {
			Collection<Thread> threads = new ArrayList<>();
			runnables.forEach((Runnable a) -> {
				Thread t = new Thread(a);
				threads.add(t);
				t.start();
			});
			return threads;
		}

		private void waitForActors(Collection<Thread> threads) throws InterruptedException {
			for (Thread t : threads) {
				t.join();
			}
		}
	}

	/**
	 * Returns the id: "P" + {@link Environment} id that this {@link Physics} resides in.
	 * 
	 * @return the inherited id
	 */
	@Override
	public String getId() {
		return "P" + environment.getId();
	}

	/**
	 * Unsupported, a {@link Physics} inherits its id from its {@link Environment}.
	 * 
	 * @throws UnsupportedOperationException
	 */
	@Override
	public void setId(String id) {
		throw new UnsupportedOperationException("Cannot explicately set the id of a Physics");
	}

	protected void setTimeState(boolean serial) {
		if (serial) {
			timestate = new TimeStateSerial();
		} else {
			timestate = new TimeStateParallel();
		}
	}

	public void setFramelength(long framelength) {
		this.framelength = framelength;
	}

	public long getFramelength() {
		return framelength;
	}

	@Override
	public void run() {
		this.simulate();
	}

	public void infoOn() {
		this.info = true;
	}

	public void infoOff() {
		this.info = false;
	}

	public static class ActionExecutor<E extends Environment<E>> {

		Map<Class<?>, RuleExecutor<?, E>> actionRuleSets;

		public ActionExecutor() {
			actionRuleSets = new HashMap<>();
		}

		public boolean containsKey(Object key) {
			return actionRuleSets.containsKey(key);
		}

		public <A extends EnvironmentalAction> void put(ActionDefinition<A, E> actionDefinition) {
			actionRuleSets.put(actionDefinition.getActionclass(), new RuleExecutor<A, E>(actionDefinition));
		}

		public void executeAction(EnvironmentalAction action, E environment) {
			if (actionRuleSets.containsKey(action.getClass())) {
				actionRuleSets.get(action.getClass()).execute(action, environment);
			} else {
				System.err.println("An agent tried to execute an action that does not exist in this environment: "
						+ environment + " - add an action definiton! : " + System.lineSeparator() + action);
			}
		}

		private static class RuleExecutor<A extends EnvironmentalAction, E extends Environment<E>> {

			private ActionDefinition<A, E> actionDefinition;

			public RuleExecutor(ActionDefinition<A, E> actionDefinition) {
				super();
				this.actionDefinition = actionDefinition;
			}

			public boolean execute(EnvironmentalAction action, E environment) {
				A raction = actionDefinition.getActionclass().cast(action);
				return actionDefinition.getRuleset().isPossible(raction, environment)
						&& actionDefinition.getRuleset().perform(raction, environment)
						&& actionDefinition.getRuleset().verify(raction, environment);
			}

			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((actionDefinition == null) ? 0 : actionDefinition.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				RuleExecutor<?, ?> other = (RuleExecutor<?, ?>) obj;
				if (actionDefinition == null) {
					if (other.actionDefinition != null)
						return false;
				} else if (!actionDefinition.equals(other.actionDefinition))
					return false;
				return true;
			}
		}
	}
}