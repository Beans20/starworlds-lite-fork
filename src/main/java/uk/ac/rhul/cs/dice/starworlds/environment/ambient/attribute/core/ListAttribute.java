package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import uk.ac.rhul.cs.dice.starworlds.utils.StringUtils;

public class ListAttribute<V> extends CollectionAttribute<V> implements List<V> {

	private List<V> list;

	public ListAttribute(List<V> list) {
		super(list);
	}

	public void add(int index, V element) {
		list.add(index, element);
	}

	public boolean add(V e) {
		return list.add(e);
	}

	public boolean addAll(Collection<? extends V> c) {
		return list.addAll(c);
	}

	public boolean addAll(int index, Collection<? extends V> c) {
		return list.addAll(index, c);
	}

	public void clear() {
		list.clear();
	}

	public boolean contains(Object o) {
		return list.contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return list.containsAll(c);
	}

	public boolean equals(Object o) {
		return list.equals(o);
	}

	public void forEach(Consumer<? super V> action) {
		list.forEach(action);
	}

	public V get(int index) {
		return list.get(index);
	}

	public int hashCode() {
		return list.hashCode();
	}

	public int indexOf(Object o) {
		return list.indexOf(o);
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

	public Iterator<V> iterator() {
		return list.iterator();
	}

	public int lastIndexOf(Object o) {
		return list.lastIndexOf(o);
	}

	public ListIterator<V> listIterator() {
		return list.listIterator();
	}

	public ListIterator<V> listIterator(int index) {
		return list.listIterator(index);
	}

	public Stream<V> parallelStream() {
		return list.parallelStream();
	}

	public V remove(int index) {
		return list.remove(index);
	}

	public boolean remove(Object o) {
		return list.remove(o);
	}

	public boolean removeAll(Collection<?> c) {
		return list.removeAll(c);
	}

	public boolean removeIf(Predicate<? super V> filter) {
		return list.removeIf(filter);
	}

	public void replaceAll(UnaryOperator<V> operator) {
		list.replaceAll(operator);
	}

	public boolean retainAll(Collection<?> c) {
		return list.retainAll(c);
	}

	public V set(int index, V element) {
		return list.set(index, element);
	}

	public int size() {
		return list.size();
	}

	public void sort(Comparator<? super V> c) {
		list.sort(c);
	}

	public Spliterator<V> spliterator() {
		return list.spliterator();
	}

	public Stream<V> stream() {
		return list.stream();
	}

	public List<V> subList(int fromIndex, int toIndex) {
		return list.subList(fromIndex, toIndex);
	}

	public Object[] toArray() {
		return list.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return list.toArray(a);
	}

	@Override
	public String toString() {
		return new StringBuilder(this.getClass().getSimpleName()).append("{")
				.append(StringUtils.collectionToString(list)).append("}").toString();
	}

}
