package uk.ac.rhul.cs.dice.starworlds.appearances.serializable;

import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;

public class SerializableActiveBodyAppearance extends SerializablePhysicalBodyAppearance {

	private static final long serialVersionUID = 2869433559504333485L;

	private Map<ComponentKey, Class<?>> components;

	public SerializableActiveBodyAppearance(ActiveBody<?> manifest) {
		super(manifest);
		this.components = manifest.getComponentClasses();
	}

	public Map<ComponentKey, Class<?>> getComponents() {
		return components;
	}

}
