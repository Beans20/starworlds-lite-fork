package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key;

import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.AgentsAttribute;

public class AgentAttributeKey extends AttributeKey<AgentsAttribute> {

	public AgentAttributeKey() {
		super(AgentsAttribute.class);
	}
}
