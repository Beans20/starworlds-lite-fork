package uk.ac.rhul.cs.dice.starworlds.entities.avatar;

import java.io.Reader;

import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimpleComponentKey;

public abstract class KeyboardAvatarMind<R extends Reader> extends InterfacedAvatarMind<R, String, SimpleComponentKey> {

	protected R reader;

	public KeyboardAvatarMind() {
	}

	public KeyboardAvatarMind(R reader) {
		this.reader = reader;
	}

	@Override
	public R getInterface() {
		return this.reader;
	}

	@Override
	public void showInterface(R reader) {
		// pass - it should already be available to the user
	}

	public class KeyboardResponse implements UserResponse<String> {

		private String data;

		public KeyboardResponse(String data) {
			// TODO Auto-generated constructor stub
		}

		@Override
		public Object[] getResponse() {
			return new Object[] { data };
		}

		@Override
		public String getKey() {
			return data;
		}
	}
}
