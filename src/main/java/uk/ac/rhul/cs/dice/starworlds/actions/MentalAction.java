package uk.ac.rhul.cs.dice.starworlds.actions;

import uk.ac.rhul.cs.dice.starworlds.entities.agent.Mind;

/**
 * The interface for all the {@link Action}s that should be attempted inside a {@link Mind}.
 * 
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface MentalAction extends Action {

}