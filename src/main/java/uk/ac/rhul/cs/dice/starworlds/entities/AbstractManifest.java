package uk.ac.rhul.cs.dice.starworlds.entities;

import uk.ac.rhul.cs.dice.starworlds.appearances.AbstractAppearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;

public abstract class AbstractManifest implements Manifest {

	private AbstractAppearance appearance;
	private String id;

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            : the unique identifier of this {@link Identifiable}.
	 * @param appearance
	 *            : the {@link Appearance} of this {@link Manifest}.
	 */
	public AbstractManifest(String id, AbstractAppearance appearance) {
		this.id = id;
		this.appearance = appearance;
	}

	@Override
	public Appearance getAppearance() {
		return appearance;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return appearance.toString();
	}

}
