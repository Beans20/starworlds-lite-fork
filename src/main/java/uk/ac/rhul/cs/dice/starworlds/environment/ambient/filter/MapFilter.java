package uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter;

import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;

public abstract class MapFilter<R> implements Filter<MapAttribute<?, ?>, R> {

	@Override
	public String toString() {
		return new StringBuilder(this.getClass().getSimpleName()).append("[").append(this.getKey()).append("]")
				.toString();
	}

}
