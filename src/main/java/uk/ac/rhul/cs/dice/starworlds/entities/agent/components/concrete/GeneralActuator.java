package uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.actions.environmental.PhysicalAction;
import uk.ac.rhul.cs.dice.starworlds.annotations.PossibleAction;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.AbstractActuator;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;

/**
 * A general {@link AbstractActuator} that is able to attempt {@link CommunicationAction}s as well as any
 * {@link PhysicalAction} (TODO when {@link PossibleAction} inheritance is finished!).
 * 
 * @author Ben Wilkins
 *
 * @param <K>
 *            {@link ComponentKey} type.
 */
@PossibleAction({ CommunicationAction.class, PhysicalAction.class })
public class GeneralActuator<K extends ComponentKey> extends AbstractActuator<K> {

	public GeneralActuator(K componentKey) {
		super(componentKey);
	}

	@Override
	public void attempt(EnvironmentalAction action) {
		this.getBody().getEnvironment().attemptAction(action);
	}

}
