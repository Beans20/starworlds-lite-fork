package uk.ac.rhul.cs.dice.starworlds.appearances.serializable;

import uk.ac.rhul.cs.dice.starworlds.entities.Manifest;

public class AbstractSerializableAppearance implements SerializableAppearance {

	private static final long serialVersionUID = -3995198462427593901L;
	private Class<?> manifestClass;
	private String id;

	public AbstractSerializableAppearance(Manifest manifest) {
		super();
		this.manifestClass = manifest.getClass();
		this.id = manifest.getId();
	}

	@Override
	public String represent() {
		return manifestClass.getSimpleName();
	}

	@Override
	public void manifest(Manifest manifest) {
		throw new UnsupportedOperationException("A serializable appearance cannot be manifested");
	}

	@Override
	public SerializableAppearance getSerializable() {
		return this;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		throw new UnsupportedOperationException("The id of a serializable appearance cannot be set");
	}

	@Override
	public String toString() {
		return represent();
	}

}
