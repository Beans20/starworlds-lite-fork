package uk.ac.rhul.cs.dice.starworlds.actions.factory;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;

public class CommunicationActionFactory implements ActionFactory<CommunicationAction> {

	/**
	 * @param args
	 *            [0] = {@link Serializable} message
	 * @param args
	 *            [1] = {@link Set} of {@link String} recipients
	 */
	@Override
	public CommunicationAction create(Object[] args) {
		if (args.length == 0) {
			return new CommunicationAction();
		} else if (args.length == 1) {
			return new CommunicationAction((Serializable) args[0]);
		} else if (args.length == 2) {
			Collection<?> col = (Collection<?>) args[1];
			Set<String> recipients = new HashSet<>();
			for (Object o : col) {
				recipients.add((String) o);
			}
			return new CommunicationAction((Serializable) args[0], recipients);
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}
}
