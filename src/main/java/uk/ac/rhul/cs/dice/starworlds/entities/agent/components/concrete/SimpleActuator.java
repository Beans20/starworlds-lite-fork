package uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;
import uk.ac.rhul.cs.dice.starworlds.annotations.PossibleAction;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.AbstractActuator;

@PossibleAction({ CommunicationAction.class })
public class SimpleActuator extends AbstractActuator<SimpleComponentKey> {

	public SimpleActuator() {
		super(SimpleComponentKey.ACTUATOR);
	}

}
