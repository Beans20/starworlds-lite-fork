package uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.Ambient;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;

public interface Filter<A, R> {

	public R filter(A attribute);

	public R filter(A attribute, Ambient ambient);

	public R filter(A attribute, Ambient ambient, SenseAction action);

	public FilterKey<A, R> getKey();

	public class FilterException extends StarWorldsRuntimeException {

		private static final long serialVersionUID = -5949160167135434795L;

		public FilterException(Filter<?, ?> filter, String message) {
			super(filter + ":" + message);
		}

	}
}
