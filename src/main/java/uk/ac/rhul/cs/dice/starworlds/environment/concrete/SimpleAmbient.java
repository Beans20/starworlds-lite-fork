package uk.ac.rhul.cs.dice.starworlds.environment.concrete;

import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.PassiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.AbstractAmbient;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AbstractPortal;

public class SimpleAmbient extends AbstractAmbient {

	public SimpleAmbient(Set<AbstractAgent<?>> agents, Set<ActiveBody<?>> activeBodies, Set<PassiveBody> passiveBodies,
			Set<AbstractAvatarAgent<?>> avatars) {
		super(agents, activeBodies, passiveBodies, avatars);
	}

	public SimpleAmbient(Set<AbstractAgent<?>> agents, Set<ActiveBody<?>> activeBodies, Set<PassiveBody> passiveBodies,
			Set<AbstractAvatarAgent<?>> avatars, Set<AbstractPortal> portals) {
		super(agents, activeBodies, passiveBodies, avatars, portals);
	}

	public SimpleAmbient(Set<AbstractAgent<?>> agents, Set<ActiveBody<?>> activeBodies, Set<PassiveBody> passiveBodies) {
		super(agents, activeBodies, passiveBodies);
	}

}
