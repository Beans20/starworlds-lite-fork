package uk.ac.rhul.cs.dice.starworlds.actions.environmental;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset.ActionRuleSet;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.ruleset.CommunicationActionRuleSet;

/**
 * An {@link EnvironmentalAction} that enables communication between {@link Agents Agents}. The action contains a
 * message and a list of recipients (IDs). The default {@link ActionRuleSet} for this action is
 * {@link CommunicationActionRuleSet}.
 */
public class CommunicationAction extends AbstractEnvironmentalAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3225521690189189896L;

	/** The recipients. */
	private Set<String> recipientsIds;

	/** The message. */
	private Serializable message = null;

	/**
	 * Constructor.
	 */
	public CommunicationAction() {
		this.recipientsIds = new HashSet<>();
	}

	/**
	 * Instantiates a new communication action.
	 *
	 * @param message
	 *            the message
	 */
	public CommunicationAction(Serializable message) {
		this.recipientsIds = new HashSet<>();
		this.message = message;
	}

	/**
	 * Instantiates a new communication action.
	 *
	 * @param message
	 *            the message
	 * @param recipientsIds
	 *            the recipients
	 */
	public CommunicationAction(Serializable message, Set<String> recipientsIds) {
		this.recipientsIds = (recipientsIds != null) ? recipientsIds : new HashSet<>();
		this.message = message;
	}

	/**
	 * Gets the recipients.
	 *
	 * @return the recipients
	 */
	public Set<String> getRecipientsIds() {
		return this.recipientsIds;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public Serializable getMessage() {
		return this.message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see uk.ac.rhul.cs.dice.starworlds.actions.environmental.AbstractEnvironmentalAction#toString()
	 */
	@Override
	public String toString() {
		return "Message: " + this.message + " to: " + (this.recipientsIds.isEmpty() ? "Everyone" : this.recipientsIds);
	}
}