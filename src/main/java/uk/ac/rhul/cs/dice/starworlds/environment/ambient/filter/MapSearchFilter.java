package uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.Ambient;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;

/**
 * A simple search filter for {@link Map}s. The filter will search for all values mapped to the given keys. Using the
 * empty constructor indicates a search for all values in the {@link Map}. The associated {@link FilterKey} is defined
 * as an internal class, {@link MapSearchKey} and the instance can be retrieved using the static field
 * {@link MapSearchFilter#KEY} or using the {@link Filter#getKey()} method on an instance of {@link MapSearchFilter}.
 * 
 * @author Ben Wilkins
 *
 */
public class MapSearchFilter extends MapFilter<Collection<?>> {

	public static final MapSearchKey KEY = new MapSearchKey();

	private Collection<?> keys;

	public MapSearchFilter(Collection<?> keys) {
		this.keys = keys;
	}

	public MapSearchFilter() {
		this.keys = null;
	}

	@Override
	public Collection<?> filter(MapAttribute<?, ?> attribute) {
		if (keys == null) {
			return attribute.values();
		} else {
			return keys.stream().map(x -> attribute.getOrDefault(x, null)).collect(Collectors.toSet());
		}
	}

	@Override
	public Collection<?> filter(MapAttribute<?, ?> attribute, Ambient ambient) {
		return null;
	}

	@Override
	public Collection<?> filter(MapAttribute<?, ?> attribute, Ambient ambient, SenseAction action) {
		return null;
	}

	@Override
	public FilterKey<MapAttribute<?, ?>, Collection<?>> getKey() {
		return KEY;
	}

	public static class MapSearchKey extends FilterKey<MapAttribute<?, ?>, Collection<?>> {
		private MapSearchKey() {
			super();
		}

		@Override
		public Collection<?> filter(Filter<MapAttribute<?, ?>, Collection<?>> filter, MapAttribute<?, ?> attribute,
				Ambient ambient, SenseAction action) {
			return filter.filter(attribute);
		}
	}
}
