package uk.ac.rhul.cs.dice.starworlds.entities.agent;

import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;

/**
 * 
 * The base interface for a Mind.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 * 
 */
public interface Mind<K extends ComponentKey> {

	/**
	 * The cycle of all {@link Mind}s. Usually consisting of (but isn't limited to) {@link AbstractAgentMind#perceive()}
	 * , {@link AbstractAgentMind#decide()} and {@link AbstractAgentMind#execute()} in that order. For an example of
	 * {@link Mind#cycle()} see {@link AbstractAgentMind#cycle()}.
	 */
	public void cycle();

	/**
	 * Setter for the {@link AbstractAgent} that this {@link Mind} resides in.
	 * 
	 * @param body
	 */
	public void setBody(AbstractAgent<K> body);

}