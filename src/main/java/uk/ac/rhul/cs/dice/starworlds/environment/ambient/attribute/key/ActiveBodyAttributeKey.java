package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key;

import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.ActiveBodiesAttribute;

public class ActiveBodyAttributeKey extends AttributeKey<ActiveBodiesAttribute> {

	public ActiveBodyAttributeKey() {
		super(ActiveBodiesAttribute.class);
	}

}
