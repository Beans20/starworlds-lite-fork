package uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.Ambient;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.Attribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key.AttributeKey;

/**
 * A {@link FilterChain} is used to determine what is sensed by a {@link SenseAction}. The initial element in the chain
 * is an {@link Ambient} {@link Attribute}, subsequent elements are {@link Filter}s. Each filter is applied to the
 * {@link Attribute} in order of append. The construction of a chain is as follows, <br>
 * 
 * <pre>
 * FilterChain.start(attributekey).append(filter1).append(filter2) ... .append(filterN);
 * </pre>
 * 
 * Filters are applied in order to the return of the prior.
 * 
 * @author Ben Wilkins
 *
 * @param <S>
 *            parameter type
 * @param <T>
 *            return type
 */
public class FilterChain<S extends Attribute, T> {

	private AttributeKey<S> attribute;
	private List<FilterKeyPair<?, ?>> filters;

	private FilterChain() {
	}

	public static <V extends Attribute> FilterChain<V, V> start(AttributeKey<V> attributeKey) {
		FilterChain<V, V> chain = new FilterChain<>();
		chain.attribute = attributeKey;
		chain.filters = new ArrayList<>();
		return chain;
	}

	public <V> FilterChain<S, V> append(Filter<T, V> filter) {
		FilterChain<S, V> chain = new FilterChain<S, V>();
		chain.attribute = attribute;
		chain.filters = new ArrayList<FilterKeyPair<?, ?>>(filters);
		chain.filters.add(new FilterKeyPair<T, V>(filter.getKey(), filter));
		return chain;
	}

	// this is safe because to only way to add to the filter list is the append method
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public T perform(Ambient ambient, SenseAction action) {
		Object target = ambient.getAttribute(attribute);
		for (FilterKeyPair<?, ?> p : filters) {
			FilterKey key = p.key;
			target = key.filter(p.filter, target, ambient, action);
		}
		return (T) target;
	}

	public AttributeKey<?> getAttribute() {
		return attribute;
	}

	public Collection<Filter<?, ?>> getFilters() {
		return filters.stream().map(FilterKeyPair::getFilter).collect(Collectors.toSet());
	}

	public Collection<FilterKey<?, ?>> getFilterKeys() {
		return filters.stream().map(FilterKeyPair::getKey).collect(Collectors.toSet());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(attribute);
		for (FilterKeyPair<?, ?> p : filters) {
			builder.append(".").append(p.key);
		}
		return builder.toString();
	}

	private static class FilterKeyPair<S, T> {
		private Filter<S, T> filter;
		private FilterKey<S, T> key;

		public FilterKeyPair(FilterKey<S, T> key, Filter<S, T> filter) {
			super();
			this.filter = filter;
			this.key = key;
		}

		private Filter<S, T> getFilter() {
			return filter;
		}

		private FilterKey<S, T> getKey() {
			return key;
		}

		@Override
		public String toString() {
			return new StringBuilder().append(key).append("->").append(filter).toString();
		}
	}
}
