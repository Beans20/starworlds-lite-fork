package uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.SenseAction;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.Ambient;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.Key;

public abstract class FilterKey<A, R> extends Key {

	public FilterKey() {
		super();
	}

	/**
	 * A {@link FilterKey} is responsible for calling the correct filter method in its associated filter. This method is
	 * one of the following (defined in the {@link Filter} interface), <br>
	 * <code> filter(A attribute) </code> or, <br>
	 * <code> filter(A attribute, Ambient ambient) </code> <br>
	 * suggested implementations of this method:
	 * 
	 * <pre>
	 * public abstract R filter(Filter<A, R> filter, A attribute, Ambient ambient) {
	 * 	filter.filter(attribute);
	 * }
	 * 
	 * public abstract R filter(Filter<A, R> filter, A attribute, Ambient ambient) {
	 * 	filter.filter(attribute, ambient);
	 * }
	 * 
	 * 	public R filter(Filter<A, R> filter, A attribute, Ambient ambient, SenseAction action) {
	 * 	filter.filter(attribute, ambient, action);
	 * }
	 * </pre>
	 * 
	 * @param filter
	 *            {@link Filter} instance used to call <code> filter(...) </code>
	 * @param attribute
	 *            to use as an argument for the <code> filter(...) </code> method
	 * @param ambient
	 *            to use as an argument for the <code> filter(...) </code> method
	 * @param action
	 *            to use as an argument for the <code> filter(...) </code> method
	 * @return
	 */
	public abstract R filter(Filter<A, R> filter, A attribute, Ambient ambient, SenseAction action);

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return new StringBuilder().append(this.getClass().getSimpleName()).append("[").append(this.key).append("]")
				.toString();
	}

}
