package uk.ac.rhul.cs.dice.starworlds.initialisation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Component;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimpleActiveSensor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimpleActuator;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimpleComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.concrete.SimplePassiveSensor;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.concrete.SimpleAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.concrete.SimpleMind;

public class AgentFactory {

	private static AgentFactory instance = new AgentFactory();

	// private static IDFactory idfactory = IDFactory.getInstance();

	private AgentFactory() {
	}

	public SimpleAgent createSimpleAgent() {
		return new SimpleAgent(getSimpleComponents(), new SimpleMind());
	}

	public Collection<Component<SimpleComponentKey>> getSimpleComponents() {
		List<Component<SimpleComponentKey>> components = new ArrayList<>();
		components.add(new SimplePassiveSensor());
		components.add(new SimpleActiveSensor());
		components.add(new SimpleActuator());
		return components;
	}

	public static AgentFactory getInstance() {
		return instance;
	}
}
