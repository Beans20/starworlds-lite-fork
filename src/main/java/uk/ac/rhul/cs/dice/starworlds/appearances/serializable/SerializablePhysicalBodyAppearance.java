package uk.ac.rhul.cs.dice.starworlds.appearances.serializable;

import uk.ac.rhul.cs.dice.starworlds.entities.PhysicalBody;

public class SerializablePhysicalBodyAppearance extends AbstractSerializableAppearance {

	private static final long serialVersionUID = -3299913862517260517L;

	public SerializablePhysicalBodyAppearance(PhysicalBody manifest) {
		super(manifest);
	}

}
