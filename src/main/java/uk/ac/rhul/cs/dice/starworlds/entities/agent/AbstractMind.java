package uk.ac.rhul.cs.dice.starworlds.entities.agent;

import uk.ac.rhul.cs.dice.starworlds.entities.Agent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;

public abstract class AbstractMind<K extends ComponentKey> implements Mind<K> {

	private AbstractAgent<K> body;

	@Override
	public void setBody(AbstractAgent<K> body) {
		if (this.body == null) {
			this.body = body;
		}
	}

	protected AbstractAgent<K> getBody() {
		return this.body;
	}

	/**
	 * Getter for the id of this {@link Agent}. This method may be used when
	 * communicating as the 'return address'.
	 * 
	 * @return the id
	 */
	protected String getId() {
		return this.body.getId();
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + this.getId();
	}

}
