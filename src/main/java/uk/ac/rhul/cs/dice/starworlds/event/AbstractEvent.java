package uk.ac.rhul.cs.dice.starworlds.event;

import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;

public abstract class AbstractEvent implements Event {

	private static final long serialVersionUID = -6385222899824701869L;

	private String id = null;
	private Identifiable origin = null;

	public AbstractEvent() {
	}

	public AbstractEvent(Identifiable origin) {
		this.origin = origin;
	}

	public AbstractEvent(String id, Identifiable origin) {
		this.origin = origin;
		this.id = (id != null) ? id : "";
	}

	@Override
	public Identifiable getOrigin() {
		return origin;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ":" + this.id;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void setId(String id) {
		throw new StarWorldsRuntimeException("The id of an event should not be set after construction");
	}
}
