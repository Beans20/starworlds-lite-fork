package uk.ac.rhul.cs.dice.starworlds.environment.physics;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.appearances.Identifiable;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.physics.actiondefinition.ActionDefinition;

/**
 * The general interface for all the physics.<br/>
 * <br/>
 * 
 * Known implementations: {@link AbstractPhysics}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface Physics<E extends Environment<E>> extends Identifiable {

	public void runActors();

	public void executeActions();

	public void executeAction(EnvironmentalAction action, E environment);

	public <A extends EnvironmentalAction> void addActionDefinition(ActionDefinition<A, E> actionDefinition);

	public void setEnvironment(E environment);

	public E getEnvironment();

	public Long getTime();

}