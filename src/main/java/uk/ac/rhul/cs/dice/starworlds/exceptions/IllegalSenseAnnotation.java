package uk.ac.rhul.cs.dice.starworlds.exceptions;

public class IllegalSenseAnnotation extends StarWorldsRuntimeException {

	private static final long serialVersionUID = 1L;

	public IllegalSenseAnnotation() {
		super("Illegal use of SenseAnnotaton: ");
	}
}
