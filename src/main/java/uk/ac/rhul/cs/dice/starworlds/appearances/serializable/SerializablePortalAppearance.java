package uk.ac.rhul.cs.dice.starworlds.appearances.serializable;

import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.Portal;

public class SerializablePortalAppearance extends AbstractSerializableAppearance {

	private static final long serialVersionUID = -4122337459622051784L;

	public SerializablePortalAppearance(Portal manifest) {
		super(manifest);
	}
}
