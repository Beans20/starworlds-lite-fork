package uk.ac.rhul.cs.dice.starworlds.environment.subscription;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

import uk.ac.rhul.cs.dice.starworlds.utils.StringUtils;

public class DefaultAnnotationInheritanceMap<C, A extends Annotation> extends AnnotationInheritanceMap<C, A> {

	/** All of the values that are in this map */
	private Set<C> values;

	public DefaultAnnotationInheritanceMap(Collection<Class<?>> keys, Class<A> annotation, Function<A, C[]> getValue,
			Class<?>... base) {
		super(annotation, a -> {
			if (a != null) {
				return Arrays.asList(getValue.apply(a));
			} else {
				return new ArrayList<>(0);
			}
		}, base);
		this.values = new HashSet<>();
		this.initialiseBase();
		keys.forEach(this::put);

	}

	public Set<C> getValues() {
		return values;
	}

	@Override
	protected Collection<C> newCollection(Class<?> key) {
		Collection<C> percepts = super.newCollection(key);
		this.values.addAll(percepts);
		return percepts;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName()).append(":").append(
				System.lineSeparator());
		map.forEach((a, b) -> builder.append("   ").append(a.getSimpleName()).append("->")
				.append(StringUtils.collectionToStringSingle(b)).append(System.lineSeparator()));
		return builder.toString();
	}
}