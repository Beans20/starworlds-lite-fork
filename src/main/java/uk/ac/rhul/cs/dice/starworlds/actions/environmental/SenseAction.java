package uk.ac.rhul.cs.dice.starworlds.actions.environmental;

import java.security.KeyStore.Entry.Attribute;
import java.util.Collection;

import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Sensor;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.Ambient;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key.AttributeKey;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter.FilterChain;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter.FilterKey;

// TODO: Auto-generated Javadoc
/**
 * An {@link AbstractEnvironmentalAction} representing sensing actions.<br/>
 * A {@link SenseAction} uses a {@link FilterChain} to attempt the sensing of a specific part ({@link Attribute}) of an
 * {@link Ambient}. A guide on constructing a {@link FilterChain} can be found in its class documentation.
 * 
 * <br/>
 * 
 * Known direct subclasses: none.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public class SenseAction extends AbstractEnvironmentalAction {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4695622233320564044L;

	/** The filter chain. */
	private FilterChain<?, ?> chain;

	/** The component key of the {@link Sensor} that attempted this {@link SenseAction}, see {@link ComponentKey}. */
	private ComponentKey componentKey;

	/**
	 * Construct.
	 * 
	 * @param chain
	 *            : to be used
	 */
	public SenseAction(FilterChain<?, ?> chain) {
		this.chain = chain;
	}

	/**
	 * Getter for {@link FilterChain}.
	 *
	 * @return the chain
	 */
	public FilterChain<?, ?> getChain() {
		return chain;
	}

	/**
	 * Getter for the {@link Attribute} to sense from the {@link FilterChain}.
	 * 
	 * @return the attribute
	 */
	public AttributeKey<?> getAttribute() {
		return chain.getAttribute();
	}

	/**
	 * Get {@link FilterKey}s from the {@link FilterChain}.
	 *
	 * @return the filter keys
	 */
	public Collection<FilterKey<?, ?>> getFilterKeys() {
		return chain.getFilterKeys();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see uk.ac.rhul.cs.dice.starworlds.actions.environmental.AbstractEnvironmentalAction#toString()
	 */
	@Override
	public String toString() {
		return new StringBuilder(this.getClass().getSimpleName()).append("[").append(chain).append("]").toString();
	}

	/**
	 * Gets the component key of the {@link Sensor} that attempted this {@link SenseAction}.
	 *
	 * @return the component key
	 */
	public ComponentKey getComponentKey() {
		return componentKey;
	}

	/**
	 * Sets the component key.
	 *
	 * @param componentKey
	 *            the new component key
	 */
	public void setComponentKey(ComponentKey componentKey) {
		this.componentKey = componentKey;
	}

}