package uk.ac.rhul.cs.dice.starworlds.environment.concrete;

import uk.ac.rhul.cs.dice.starworlds.environment.physics.AbstractPhysics;

public class SimplePhysics extends AbstractPhysics<SimpleUniverse> {

	public SimplePhysics() {
		super();
	}

	public SimplePhysics(Time time) {
		super(time);
	}

	@Override
	public void cycleAddition() {
		//no addition
	}

}
