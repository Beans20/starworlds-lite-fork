package uk.ac.rhul.cs.dice.starworlds.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SensiblePerception {
	Class<? extends Perception>[] value() default {};
}