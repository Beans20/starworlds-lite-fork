package uk.ac.rhul.cs.dice.starworlds.entities;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.annotations.PossibleActionDiscovery;
import uk.ac.rhul.cs.dice.starworlds.appearances.ActiveBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAutonomousAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Actuator;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Component;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.ComponentKey;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.components.Sensor;
import uk.ac.rhul.cs.dice.starworlds.environment.AbstractEnvironment;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.exceptions.StarWorldsRuntimeException;
import uk.ac.rhul.cs.dice.starworlds.perception.Perception;

/**
 * A subclass of {@link PhysicalBody} capable to perform an {@link EnvironmentalAction}, thus implementing {@link Actor}
 * .
 * 
 * Known direct subclasses: {@link AbstractAutonomousAgent}, {@link DependentBody}.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class ActiveBody<K extends ComponentKey> extends PhysicalBody implements Actor {

	private Set<Sensor<K>> activatedSensors;
	private Set<Actuator<K>> activatedActuators;
	private Map<K, Component<K>> components;
	private Environment<?> environment;

	/**
	 * Constructor. The default {@link Appearance} for an {@link ActiveBody} is created automatically.
	 * 
	 * @param id
	 *            :
	 * @param components
	 *            : collection of {@link Sensor}s and {@link Actuator}s to be used by this {@link ActiveBody}
	 */
	public ActiveBody(String id, Collection<Component<K>> components) {
		super(id, new ActiveBodyAppearance());
		init(components);
		this.getAppearance().manifest(this);
	}

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            :
	 * @param appearance
	 *            : the {@link Appearance} of the {@link ActiveBody}.
	 * @param components
	 *            : collection of {@link Sensor}s and {@link Actuator}s to be used by this {@link ActiveBody}
	 */
	public ActiveBody(String id, ActiveBodyAppearance appearance, Collection<Component<K>> components) {
		super(id, appearance);
		init(components);
	}

	private void init(Collection<Component<K>> components) {
		this.activatedSensors = new HashSet<>();
		this.activatedActuators = new HashSet<>();
		this.components = mapComponents(components);
		components.forEach(x -> x.setBody(this));
	}

	public <V extends Component<K>> Map<K, V> mapComponents(Collection<V> components) {
		return components.stream().collect(Collectors.toMap(Component::getComponentKey, Function.identity()));
	}

	public Component<K> getComponent(ComponentKey componentKey) {
		return this.components.get(componentKey);
	}

	public Sensor<K> getSensor(ComponentKey componentKey) {
		return (Sensor<K>) this.components.get(componentKey);
	}

	public Actuator<K> getActuator(ComponentKey componentKey) {
		return (Actuator<K>) this.components.get(componentKey);
	}

	public Collection<Component<K>> getComponents() {
		return this.components.values();
	}

	public Map<K, Component<K>> getComponentsMap() {
		return this.components;
	}

	public Set<Class<?>> getSensorClasses() {
		return this.components.values().stream().filter(x -> x instanceof Sensor).map(Component::getClass)
				.collect(Collectors.toSet());
	}

	public Set<Class<?>> getActuatorClasses() {
		return this.components.values().stream().filter(x -> x instanceof Sensor).map(Component::getClass)
				.collect(Collectors.toSet());
	}

	public Collection<Sensor<?>> getSensors() {
		return this.components.values().stream().filter(x -> x instanceof Sensor).map(Sensor.class::cast)
				.collect(Collectors.toSet());
	}

	public Collection<Actuator<?>> getActuators() {
		return this.components.values().stream().filter(x -> x instanceof Actuator).map(Actuator.class::cast)
				.collect(Collectors.toSet());
	}

	public Collection<K> getComponentKeys() {
		return this.components.keySet();
	}

	/**
	 * Adds the given {@link Component} to this body.
	 * 
	 * @param component
	 */
	public void addComponent(Component<K> component) {
		this.components.put(component.getComponentKey(), component);
		component.setBody(this);
	}

	public Collection<Perception> perceive() {
		Set<Perception> perceptions = new HashSet<>();
		this.activatedSensors.forEach(s -> perceptions.addAll(s.getPerceptions()));
		this.activatedSensors.clear();
		return perceptions;
	}

	// TODO I am still unsure about execute? should it not be attempt? - Ben
	public void attempt(Map<K, ? extends EnvironmentalAction> actions) {
		actions.forEach(this::attempt);
	}

	protected void attempt(K componentKey, EnvironmentalAction action) {
		Component<K> component = this.getComponent(componentKey);
		if (component instanceof Actuator) {
			action.setActor(this.getAppearance());
			Actuator<?> actuator = ((Actuator<?>) component);
			if (PossibleActionDiscovery.getPossibleActions(actuator.getClass()).contains(action.getClass())) {
				actuator.attempt(action);
			} else {
				possibleActionError(actuator, action);
			}
		} else {
			isActuatorError(component, action);
		}
	}

	public Map<ComponentKey, Class<?>> getComponentClasses() {
		return components.entrySet().stream()
				.collect(Collectors.toMap(this::getComponentKey, this::getComponentClass, (a, b) -> a, HashMap::new));
	}

	private ComponentKey getComponentKey(Map.Entry<?, ?> entry) {
		return (ComponentKey) entry.getKey();
	}

	private Class<?> getComponentClass(Map.Entry<?, ?> entry) {
		return entry.getValue().getClass();
	}

	public Collection<Class<? extends EnvironmentalAction>> getPossibleActions(K componentKey) {
		return PossibleActionDiscovery.getPossibleActions(this.getComponent(componentKey).getClass());
	}

	public void sensorActive(Sensor<K> sensor) {
		this.activatedSensors.add(sensor);
	}

	public void actuatorActive(Actuator<K> actuator) {
		this.activatedActuators.add(actuator);
	}

	/**
	 * Getter for the {@link AbstractEnvironment} that this {@link ActiveBody} resides in.
	 * 
	 * @return the {@link AbstractEnvironment}
	 */
	public Environment<?> getEnvironment() {
		return environment;
	}

	/**
	 * Setter for the {@link AbstractEnvironment} that this {@link ActiveBody} resides in.
	 * 
	 * @param environment
	 *            : to set
	 */
	public void setEnvironment(AbstractEnvironment<?> environment) {
		this.environment = environment;
	}

	@Override
	public ActiveBodyAppearance getAppearance() {
		return (ActiveBodyAppearance) super.getAppearance();
	}

	private void possibleActionError(Actuator<?> actuator, EnvironmentalAction action) {
		StringBuilder builder = new StringBuilder("Failed to attempt action: ")
				.append(action)
				.append(" using component: ")
				.append(actuator)
				.append(", with key: ")
				.append(actuator.getComponentKey())
				.append(System.lineSeparator())
				.append("this action is not one that is possible with this actuator, its possible actions are as follows:")
				.append(System.lineSeparator());
		Collection<Class<? extends EnvironmentalAction>> actions = PossibleActionDiscovery.getPossibleActions(actuator
				.getClass());
		if (actions == null || actions.isEmpty()) {
			builder.append("NONE, perhaps the PossibleAction annotation was not used for this actuator type?");
		} else {
			actions.forEach(x -> builder.append(x).append(System.lineSeparator()));
		}
		throw new StarWorldsRuntimeException(builder.toString());
	}

	private void isActuatorError(Component<?> component, EnvironmentalAction action) {
		StringBuilder builder = new StringBuilder("Failed to attempt action: ").append(action)
				.append(" using component: ").append(component).append(" with key: ")
				.append(component.getComponentKey()).append(" this component is not an actuator.");
		throw new StarWorldsRuntimeException(builder.toString());
	}
}