package uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal;

import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.PortalAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.AbstractManifest;

public abstract class AbstractPortal extends AbstractManifest implements Portal {

	private Appearance remote;

	public AbstractPortal(String id, PortalAppearance appearance) {
		super(id, appearance);
		appearance.manifest(this);
	}

	@Override
	public final void setId(String id) {
		throw new UnsupportedOperationException("Cannot reset the id of " + this.getClass().getSimpleName());
	}

	@Override
	public Appearance getRemoteAppearance() {
		return remote;
	}

	public void setRemote(Appearance remote) {
		this.remote = remote;
	}
}
