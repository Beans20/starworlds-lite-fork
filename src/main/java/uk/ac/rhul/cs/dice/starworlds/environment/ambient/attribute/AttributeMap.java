package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;

import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key.AttributeKey;

public class AttributeMap {

	private HashMap<AttributeKey<?>, Attribute> attributes;

	public AttributeMap() {
		this.attributes = new HashMap<AttributeKey<?>, Attribute>();
	}

	public void clear() {
		attributes.clear();
	}

	public boolean containsKey(AttributeKey<?> key) {
		return attributes.containsKey(key);
	}

	public boolean containsValue(Attribute value) {
		return attributes.containsValue(value);
	}

	public Set<Entry<AttributeKey<?>, Attribute>> entrySet() {
		return attributes.entrySet();
	}

	public void forEach(BiConsumer<? super AttributeKey<?>, ? super Attribute> action) {
		attributes.forEach(action);
	}

	public <V extends Attribute> V get(AttributeKey<V> key) {
		return key.getAttributeClass().cast(attributes.get(key));
	}

	public boolean isEmpty() {
		return attributes.isEmpty();
	}

	public Set<AttributeKey<?>> keySet() {
		return attributes.keySet();
	}

	public <V extends Attribute> void put(AttributeKey<V> key, V value) {
		if (!attributes.containsKey(key)) {
			attributes.put(key, value);
		} else {
			throw new AttributeException("Cannot override attribute: " + attributes.get(key) + " with " + value);
		}

	}

	public <V extends Attribute> boolean remove(AttributeKey<V> key, V value) {
		return attributes.remove(key, value);
	}

	public void remove(AttributeKey<?> key) {
		attributes.remove(key);
	}

	public <V extends Attribute> void replace(AttributeKey<V> key, V value) {
		attributes.replace(key, value);
	}

	public int size() {
		return attributes.size();
	}

	public String toString() {
		return "Ambient Attributes: " + attributes.toString();
	}

	public Collection<Attribute> values() {
		return attributes.values();
	}
}
