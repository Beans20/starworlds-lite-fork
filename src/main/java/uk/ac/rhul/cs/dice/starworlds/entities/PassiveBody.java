package uk.ac.rhul.cs.dice.starworlds.entities;

import uk.ac.rhul.cs.dice.starworlds.appearances.Appearance;
import uk.ac.rhul.cs.dice.starworlds.appearances.PhysicalBodyAppearance;

/**
 * A subclass of {@link PhysicalBody} which implements {@link ObjectInterface}.<br/>
 * <br/>
 * 
 * Known direct subclasses: none.
 * 
 * @author cloudstrife9999 a.k.a. Emanuele Uliana
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public abstract class PassiveBody extends PhysicalBody {

	/**
	 * Constructor.
	 * 
	 * @param id
	 *            : the unique identifier of this {@link PassiveBody}.
	 * @param appearance
	 *            : the {@link Appearance} of this {@link PassiveBody}.
	 */
	public PassiveBody(String id, PhysicalBodyAppearance appearance) {
		super(id, appearance);
	}
}