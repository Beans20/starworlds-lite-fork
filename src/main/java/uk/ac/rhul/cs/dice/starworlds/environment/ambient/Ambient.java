package uk.ac.rhul.cs.dice.starworlds.environment.ambient;

import java.util.Collection;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.EnvironmentalAction;
import uk.ac.rhul.cs.dice.starworlds.entities.ActiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.Agent;
import uk.ac.rhul.cs.dice.starworlds.entities.PassiveBody;
import uk.ac.rhul.cs.dice.starworlds.entities.agent.AbstractAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarAgent;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.Avatar;
import uk.ac.rhul.cs.dice.starworlds.environment.AbstractEnvironment;
import uk.ac.rhul.cs.dice.starworlds.environment.Environment;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.Attribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key.AttributeKey;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.filter.FilterKey;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.AbstractPortal;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.portal.Portal;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.processes.Process;

/**
 * The interface for ambients.<br>
 * //TODO <br>
 * 
 * @author Ben Wilkins
 * @author Kostas Stathis
 *
 */
public interface Ambient {

	/**
	 * Adds the given {@link Agent} to this {@link Ambient}. This method should only be called outside of out the
	 * {@link Environment} cycle to prevent conflict of access. This method does not subscribe the {@link Agent} to the
	 * {@link Environment}, use {@link AbstractEnvironment#addAgent(AbstractAgent)}.
	 * 
	 * @param agent
	 *            : to add
	 */
	public void addAgent(AbstractAgent<?> agent);

	/**
	 * Adds the given {@link ActiveBody} to this {@link Ambient}. This method should only be called outside of out the
	 * {@link Environment} cycle to prevent conflict of access. This method does not subscribe the {@link ActiveBody} to
	 * the {@link Environment}, use {@link AbstractEnvironment#addActiveBody(ActiveBody)}.
	 * 
	 * @param body
	 *            : to add
	 */
	public void addActiveBody(ActiveBody<?> body);

	/**
	 * Adds the given {@link PassiveBody} to this {@link Ambient}. This method should only be called outside of out the
	 * {@link Environment} cycle to prevent conflict of access. This method does not subscribe the {@link PassiveBody}
	 * to the {@link Environment}, use {@link AbstractEnvironment#addAgent(AbstractAgent)}.
	 * 
	 * @param body
	 *            : to add
	 */
	public void addPassiveBody(PassiveBody body);

	/**
	 * Adds the given {@link AbstractAvatarAgent} to this {@link Ambient}. This method should only be called outside of
	 * out the {@link Environment} cycle to prevent conflict of access. This method does not subscribe the
	 * {@link Avatar} to the {@link Environment}, use {@link AbstractEnvironment#addAgent(AbstractAgent)}.
	 * 
	 * @param avatar
	 *            : to add
	 */
	public void addAvatar(AbstractAvatarAgent<?> avatar);

	public void addPortal(AbstractPortal portal);

	public Collection<AbstractAvatarAgent<?>> getAvatars();

	public Collection<AbstractAgent<?>> getAgents();

	public Collection<ActiveBody<?>> getActiveBodies();

	public Collection<PassiveBody> getPassiveBodies();

	public Collection<AbstractPortal> getPortals();

	public Collection<Process> getProcesses();

	public Portal getPortal(String id);

	public Process getProcess(String id);

	public ActiveBody<?> getActiveBody(String id);

	public AbstractAgent<?> getAgent(String id);

	public AbstractAvatarAgent<?> getAvatar(String id);

	public PassiveBody getPassiveBody(String id);

	// *** FILTERING AND ATTRIBUTES *** ///

	public Collection<AttributeKey<?>> getAttributeKeys();

	public Collection<Attribute> getAttributes();

	public <V extends Attribute> void addAttibute(AttributeKey<V> key, V attribute);

	public <V extends Attribute> V getAttribute(AttributeKey<V> key);

	public boolean attributeExists(AttributeKey<?> key);

	public Collection<FilterKey<?, ?>> getFilterKeys();

	public <A, R> void addFilter(FilterKey<A, R> key);

	public boolean filterExists(FilterKey<?, ?> key);

	// *** ACTION DEFINITIONS //NOT SO SURE ABOUT THIS? they should be defined in the physics? *** ///

	public void addActionDefinition(Class<? extends EnvironmentalAction> definition);

	public void addActionDefinitions(Collection<Class<? extends EnvironmentalAction>> definitions);

	public Set<Class<? extends EnvironmentalAction>> getActionDefinitions();

	public boolean actionDefined(Class<? extends EnvironmentalAction> action);

}