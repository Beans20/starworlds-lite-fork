package uk.ac.rhul.cs.dice.starworlds.entities.avatar.concrete;

import java.util.HashSet;
import java.util.Set;

import uk.ac.rhul.cs.dice.starworlds.actions.environmental.CommunicationAction;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.AbstractAvatarMind;
import uk.ac.rhul.cs.dice.starworlds.entities.avatar.UserResponse;

/**
 * A concrete {@link UserResponse} that is used to construct a {@link CommunicationAction}. The action key (for the
 * actionmap in an {@link AbstractAvatarMind}) is by default "message".
 * 
 * @author Ben Wilkins
 *
 */
public class CommunicativeResponse implements UserResponse<String> {

	public static final String ACTIONKEY = "message";
	private String message;
	private Set<String> recipients;

	/**
	 * Constructs a {@link UserResponse} from {@link String} data that represents a {@link CommunicationAction}. The
	 * string should take the following form:
	 * 
	 * </br> "message" e.g. "hello world" </br> "message -> reciepients" e.g. "hello -> 0"
	 * 
	 * @param data
	 *            to process
	 */
	public CommunicativeResponse(String data) {
		String[] split = data.split("->");
		if (split.length == 1) {
			message = split[0].trim();
			recipients = new HashSet<>(0);
		} else if (split.length == 2) {
			message = split[0].trim();
			String[] rsplit = split[1].split(",");
			recipients = new HashSet<>();
			for (String recip : rsplit) {
				recipients.add(recip.replace(" ", ""));
			}
		}
	}

	@Override
	public Object[] getResponse() {
		return new Object[] { message, recipients };
	}

	/**
	 * Defaults to "message"
	 */
	@Override
	public String getKey() {
		return ACTIONKEY;
	}
}
