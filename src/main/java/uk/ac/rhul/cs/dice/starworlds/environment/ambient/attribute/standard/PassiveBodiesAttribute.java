package uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard;

import java.util.Map;

import uk.ac.rhul.cs.dice.starworlds.entities.PassiveBody;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.core.MapAttribute;

public class PassiveBodiesAttribute extends MapAttribute<String, PassiveBody> {
	public PassiveBodiesAttribute(Map<String, PassiveBody> attribute) {
		super(attribute);
	}

	@Override
	public String toString() {
		return new StringBuilder("PassiveBodies: ").append(System.lineSeparator()).append(super.toString("  ")).toString();
	}
}
