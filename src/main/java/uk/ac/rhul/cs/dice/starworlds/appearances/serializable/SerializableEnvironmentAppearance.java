package uk.ac.rhul.cs.dice.starworlds.appearances.serializable;

import uk.ac.rhul.cs.dice.starworlds.environment.AbstractEnvironment;

public class SerializableEnvironmentAppearance extends AbstractSerializableAppearance {

	private static final long serialVersionUID = -8605644537130170857L;

	public SerializableEnvironmentAppearance(AbstractEnvironment<?> environment) {
		super(environment);
	}
}
