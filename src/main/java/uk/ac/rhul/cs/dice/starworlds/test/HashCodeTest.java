package uk.ac.rhul.cs.dice.starworlds.test;

import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.key.AttributeKey;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.ActiveBodiesAttribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.AgentsAttribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.AvatarsAttribute;
import uk.ac.rhul.cs.dice.starworlds.environment.ambient.attribute.standard.PassiveBodiesAttribute;

public class HashCodeTest {

	public static void main(String[] args) {
		AttributeKey<AgentsAttribute> key1 = new AttributeKey<AgentsAttribute>(AgentsAttribute.class) {
		};
		AttributeKey<AvatarsAttribute> key2 = new AttributeKey<AvatarsAttribute>(AvatarsAttribute.class) {
		};
		AttributeKey<ActiveBodiesAttribute> key3 = new AttributeKey<ActiveBodiesAttribute>(ActiveBodiesAttribute.class) {
		};
		AttributeKey<PassiveBodiesAttribute> key4 = new AttributeKey<PassiveBodiesAttribute>(
				PassiveBodiesAttribute.class) {
		};
		AttributeKey<PassiveBodiesAttribute> key5 = new AttributeKey<PassiveBodiesAttribute>(
				PassiveBodiesAttribute.class) {
		};
		System.out.println(key1.hashCode());
		System.out.println(key2.hashCode());
		System.out.println(key3.hashCode());
		System.out.println(key4.hashCode());
		System.out.println(key5.hashCode());

	}
}
