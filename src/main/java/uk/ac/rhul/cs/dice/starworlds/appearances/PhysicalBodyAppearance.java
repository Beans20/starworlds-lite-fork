package uk.ac.rhul.cs.dice.starworlds.appearances;

import uk.ac.rhul.cs.dice.starworlds.appearances.serializable.SerializablePhysicalBodyAppearance;
import uk.ac.rhul.cs.dice.starworlds.entities.PhysicalBody;

public class PhysicalBodyAppearance extends AbstractAppearance {

	public PhysicalBodyAppearance() {
		super();
	}

	public PhysicalBodyAppearance(PhysicalBody body) {
		super(body);
	}

	@Override
	public SerializablePhysicalBodyAppearance getSerializable() {
		return new SerializablePhysicalBodyAppearance((PhysicalBody) this.getManifest());
	}
}