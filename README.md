## STARWORLDS-LITE

Starworlds is an inprogress agent development and simulation platform. Only simple (non distributed) environments can be developed in the lite version. 
Some of the relevant concepts are discussed briefly below.

###  Agents
Agents are an example of an Active Body, they are composed on a number of components - mind, body, actuators and sensors.
##### Mind
The mind of an agent is repsonsible for the agents decision making, it takes perceptions as input and outputs actions.
#####  Body
The body is the manifestation of the agent in its environment. It also provides a bridge between the agents mind and its sensors/actuators.
#####  Sensors
An agent can have many sensors, each of which subscribes to receive (sense) certain types of perceptions - e.g. a *hearing* sensor may subcribe to only *sound* percepts.
#####   Actuators
An agent can have many actuators, actions that the agent wishes to attempt are attempted via its actuators. Only certain actuators can attempt certain types of actions - e.g. a *motor* actuator may only attempt the action *turn*. 

### Actions and Perceptions
Actions and perceptions are types of events.
Actions are attempted by active bodies (agents/avatars) in an environment. The environments physics processes incomming actions changes the environments state accordingly.
Perceptions are generated as a result of changes in the environment state and are sent to any subscribing sensors.

### Environment
The environment has a state (ambient) and a physics which determines how it will evolve.

### Avatars
Avatars are another example of active bodies, the differ from agents in one key way - they are controlled by an external entity - e.g. a user. 
The user is presented with an interface (which visualises any incoming perceptions), the user decides upon actions which the avatars actuators attempt.

###  Processes (in progress)
Processes are a specific kind of event generators. They are a mechanism by which external programs can influence an environment.

### Portals (in progress)
Protals allow events (and entities) to travel between environments.

### Distrbuted Environments (in progress) 
Environments may be distributed - an environment can be split up and run on different machines. Agents are able to move seemlessly between machines via portals as if they existed on a single machine in a single environment.

NOTE: Some of the Java-doc is outdated and may not be accurate.

####  RECOMMENDED CONFIGURATION

* Eclipse (latest version, currently [2017] Oxygen) --> https://www.eclipse.org/downloads/
* Apache Maven (lasest version, currently [2017] 3.5.0) --> https://maven.apache.org/download.cgi
* m2e (from Eclipse -> Help -> Install New Software)
* git (see below) 
* if GNU/Linux --> "sudo apt-get install git" [debian/ubuntu/...] or "sudo pacman -S git" [arch/...]
* if MAC OS X  --> https://git-scm.com/download/mac  
* if Windows --> from Eclipse -> Help -> Install New Software --> egit

* sonarlint --> from Eclipse -> Help -> Eclipse Marketplace -> sonarlint